#!/bin/sh

do_test () {
	local args=$1

	audition ${args}

	exit_status=$?
	if [ ${exit_status} -ne 0 ]; then
		${SETCOLOR_FAILURE}; echo "fail!"; ${SETCOLOR_NORMAL};
		abort="true"
	fi

	sleep 1
}

do_test "bogus_path"
do_test "bogus_path"
do_test "bogus/path"
do_test "bogus/path"
do_test "another/bogus/path"
do_test "another/bogus/path"

wavs=`locate -n 1 -r "\.wav$"`
echo "wavs="$wavs
