#!/bin/sh

echo 'Generating files...'
touch NEWS README AUTHORS ChangeLog
libtoolize --automake
aclocal
autoheader -Wall
automake --gnu --add-missing -Wall
autoconf
touch Makefile.in

