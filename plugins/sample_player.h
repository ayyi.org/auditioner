/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2010-2018 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  ----------------------------------------------------------------

  based on code from:

  Sonic Visualiser
  An audio file viewer and annotation editor.
  Centre for Digital Music, Queen Mary, University of London.
  This file copyright 2006 Chris Cannam.

*/

#ifndef _SAMPLE_PLAYER_H_
#define _SAMPLE_PLAYER_H_

#define USE_ADECODER

#define DSSI_API_LEVEL 2

#include "config.h"
#include <cstdlib>
#include <string>
#include <glib.h>

#include <ladspa.h>
#include "plugin/api/dssi.h"

#include <vector>


class SamplePlayer
{
public:
	static const DSSI_Descriptor* getDescriptor(unsigned long index);

private:
	SamplePlayer(int sampleRate);
	~SamplePlayer();

	enum {
		Output1Port   = 0,
		Output2Port   = 1,
		RetunePort    = 2,
		BasePitchPort = 3,
		ConcertAPort  = 4,
		SustainPort   = 5,
		ReleasePort   = 6,
		PortCount     = 7
	};

	enum {
		Polyphony = 128
	};

    static const char* const                   portNames[PortCount];
    static const LADSPA_PortDescriptor         ports[PortCount];
    static const LADSPA_PortRangeHint          hints[PortCount];
    static const LADSPA_Properties             properties;
    static const LADSPA_Descriptor             ladspaDescriptor;
    static const DSSI_Descriptor               dssiDescriptor;
    static const DSSI_Host_Descriptor*         hostDescriptor;

    static LADSPA_Handle instantiate           (const LADSPA_Descriptor*, unsigned long);
    static void          connectPort           (LADSPA_Handle, unsigned long, LADSPA_Data*);
    static void          activate              (LADSPA_Handle);
    static void          run                   (LADSPA_Handle, unsigned long);
    static void          deactivate            (LADSPA_Handle);
    static void          cleanup               (LADSPA_Handle);
    static char*         configure             (LADSPA_Handle, const char*, const char*);
    static const DSSI_Program_Descriptor* getProgram(LADSPA_Handle, unsigned long);
    static void          selectProgram         (LADSPA_Handle, unsigned long, unsigned long);
    static int           getMidiController     (LADSPA_Handle, unsigned long);
    static void          runSynth              (LADSPA_Handle, unsigned long, snd_seq_event_t*, unsigned long);
    static void          receiveHostDescriptor (const DSSI_Host_Descriptor*);
    static void          workThreadCallback    (LADSPA_Handle);

	static void          debug_printf          (const char* class_, const char* func, int level, const char* format, ...);

	void                 update_dir            (struct stat*);
    void                 searchSamples         ();
#ifdef STREAMING
    void                 load_sample_data      (std::string path);
#else
    void                 loadSampleData        (std::string path);
#endif
    void                 fill_buffer           ();
    void                 clear_buffer          ();
    void                 runImpl               (unsigned long, snd_seq_event_t*, unsigned long);
#ifdef STREAMING
    void                 feed_ringbuffer       (int, unsigned long, unsigned long);
#else
    void                 addSample             (int, unsigned long, unsigned long);
#endif
	void                 process_event         (snd_seq_event_t*);

    float*      output1;
    float*      output2;
    float*      retune;
    float*      base_pitch;
    float*      concert_a;
    float*      sustain;
    float* m_release;

    typedef struct {
        int   n_floats;
        int   n_bytes;
        int   n_floats_per_channel;
        int   n_bytes_per_channel;
        float data;
    } Buffer;
    Buffer*     buffer;

#ifndef STREAMING
    float*      sample_data;
    size_t      m_sampleCount;
#endif
    int         samplerate;
	//int         n_channels;
#ifdef STREAMING
	jack_ringbuffer_t* rb[2];
#ifdef USE_ADECODER
	WfDecoder   decoder;
#else
	SNDFILE*    file;
	SF_INFO     sf_info;
#endif
	SRC_DATA    src_data;
	SRC_STATE*  src_state;
	bool        disk_read_complete;
#endif

    long        ons        [Polyphony]; // start time
    long        offs       [Polyphony];
    int         velocities [Polyphony];
    long        sample_num;

	time_t      dir_stat_time;
    std::string m_sampleDir;
    std::string m_program;
    std::vector<std::pair<std::string, std::string> > m_samples; // program name, path
    bool        m_sampleSearchComplete;
    int         pending_program_change;

#ifdef STREAMING
	bool        buffer_read_pending;
	bool        pending_rewind;
#endif

	GMutex      mutex;
	void        lock();
	void        unlock();

	std::vector<snd_seq_event_t> deferred_events;
};


#endif
