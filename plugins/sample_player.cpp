/*
  This file is part of the Ayyi Project. http://ayyi.org
  copyright (C) 2011-2018 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  ----------------------------------------------------------------

  Based on code from Sonic Visualiser
  An audio file viewer and annotation editor.
  Centre for Digital Music, Queen Mary, University of London.

  ----------------------------------------------------------------

  Based on trivial_sampler from the DSSI distribution
  (by Chris Cannam, public domain).

*/
#include "config.h"
#include "system.h"

#include <cmath>
#include <cstdlib>
#include <string.h>
#include <sys/stat.h>
#include <glib.h>

#include <sndfile.h>
#include <samplerate.h>
#include <iostream>
#include <jack/ringbuffer.h>

#undef DEBUG_SAMPLE_PLAYER
#define USE_ADECODER

#ifdef USE_ADECODER
extern "C" {
#include <decoder/ad.h>
}
#endif

#include "sample_player.h"

#define BLOCK_N_FRAMES 4096

#ifdef DEBUG_SAMPLE_PLAYER
guint total_played = 0;
guint total_written = 0;
#endif

extern "C" {
	int wf_debug = 0;
	#include "debug/debug_.h"
	#undef dbg
	#define dbg(L, B, ...) debug_printf("SamplePlayer", __func__, L, B, ##__VA_ARGS__)
	void pchar(int level, const char* format, ...);
	int debug = 0;
	gboolean prefer_stdout = false;
}


const DSSI_Descriptor*
dssi_descriptor(unsigned long index)
{
	return SamplePlayer::getDescriptor(index);
}


const char *const
SamplePlayer::portNames[PortCount] =
{
    "Output",
    "Output",
    "Tuned (on/off)",
    "Base Pitch (MIDI)",
    "Tuning of A (Hz)",
    "Sustain (on/off)",
    "Release time (s)"
};


const LADSPA_PortDescriptor 
SamplePlayer::ports[PortCount] =
{
    LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO,
    LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO,
    LADSPA_PORT_INPUT  | LADSPA_PORT_CONTROL,
    LADSPA_PORT_INPUT  | LADSPA_PORT_CONTROL,
    LADSPA_PORT_INPUT  | LADSPA_PORT_CONTROL,
    LADSPA_PORT_INPUT  | LADSPA_PORT_CONTROL
};

const LADSPA_PortRangeHint 
SamplePlayer::hints[PortCount] =
{
    { 0, 0, 0 },
    { 0, 0, 0 },
    { LADSPA_HINT_DEFAULT_MAXIMUM | LADSPA_HINT_INTEGER |
      LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE, 0, 1 },
    { LADSPA_HINT_DEFAULT_MIDDLE | LADSPA_HINT_INTEGER |
      LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE, 0, 120 },
    { LADSPA_HINT_DEFAULT_440 | LADSPA_HINT_LOGARITHMIC |
      LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE, 400, 499 },
    { LADSPA_HINT_DEFAULT_MINIMUM | LADSPA_HINT_INTEGER |
      LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE, 0, 1 },
    { LADSPA_HINT_DEFAULT_MINIMUM | LADSPA_HINT_LOGARITHMIC |
      LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE, 0.001, 2.0 }
};

const LADSPA_Properties
SamplePlayer::properties = LADSPA_PROPERTY_HARD_RT_CAPABLE;

const LADSPA_Descriptor 
SamplePlayer::ladspaDescriptor =
{
    0, // "Unique" ID
    "sample_player", // Label
    properties,
    "Auditioner", // Name
    "Chris Cannam", // Maker
    "GPL", // Copyright
    PortCount,
    ports,
    portNames,
    hints,
    0, // Implementation data
    instantiate,
    connectPort,
    activate,
    run,
    0, // Run adding
    0, // Set run adding gain
    deactivate,
    cleanup
};

const DSSI_Descriptor 
SamplePlayer::dssiDescriptor =
{
    2, // DSSI API version
    &ladspaDescriptor,
    configure,
    getProgram,
    selectProgram,
    getMidiController,
    runSynth,
    0, // Run synth adding
    0, // Run multiple synths
    0, // Run multiple synths adding
    receiveHostDescriptor
};

const DSSI_Host_Descriptor* SamplePlayer::hostDescriptor = 0;


const DSSI_Descriptor*
SamplePlayer::getDescriptor(unsigned long index)
{
    if (index == 0) return &dssiDescriptor;
    return 0;
}

SamplePlayer::SamplePlayer(int sampleRate) :
    output1(0),
    output2(0),
    retune(0),
    base_pitch(0),
    concert_a(0),
    sustain(0),
    m_release(0),
#ifdef STREAMING
#ifdef USE_ADECODER
	decoder{0},
#else
	file(0),
#endif
	rb{0},
	src_state(0),
#else
    sample_data(0),
    m_sampleCount(0),
#endif
    samplerate(sampleRate),
    sample_num(0),
    m_sampleDir("samples"),
    m_sampleSearchComplete(false),
    pending_program_change(-1)
{
	g_mutex_init(&mutex);
}

SamplePlayer::~SamplePlayer()
{
#ifdef STREAMING
#ifdef USE_ADECODER
	ad_close(&decoder);
#else
	if(file){ 
		sf_close(file);
		file = 0;
	}
#endif
#if 0
	if(rb1){ 
		jack_ringbuffer_free(rb1);
		jack_ringbuffer_free(rb2);
		rb1 = 0;
		rb2 = 0;
	}
#endif
#else
    if (sample_data) free(sample_data);
#endif
}
    
LADSPA_Handle
SamplePlayer::instantiate(const LADSPA_Descriptor*, unsigned long rate)
{
	if (!hostDescriptor || !hostDescriptor->request_non_rt_thread) {
		std::cerr << "SamplePlayer::instantiate: Host does not provide request_non_rt_thread, not instantiating" << std::endl;
		return 0;
	}

	SamplePlayer* player = new SamplePlayer(rate);

	if (hostDescriptor->request_non_rt_thread(player, workThreadCallback)) {
		std::cerr << "SamplePlayer::instantiate: Host rejected request_non_rt_thread call, not instantiating" << std::endl;
		delete player;
		return 0;
	}

	return player;
}

void
SamplePlayer::connectPort(LADSPA_Handle handle, unsigned long port, LADSPA_Data* location)
{
	SamplePlayer* player = (SamplePlayer*)handle;

	float** ports[PortCount] = {
		&player->output1,
		&player->output2,
		&player->retune,
		&player->base_pitch,
		&player->concert_a,
		&player->sustain,
		&player->m_release
	};

	*ports[port] = (float *)location;
}

void
SamplePlayer::activate(LADSPA_Handle handle)
{
	SamplePlayer* player = (SamplePlayer*)handle;
	player->lock();

	player->sample_num = 0;

	for (size_t i = 0; i < Polyphony; ++i) {
		player->ons[i] = -1;
		player->offs[i] = -1;
		player->velocities[i] = 0;
	}
	player->unlock();
}

void
SamplePlayer::run(LADSPA_Handle handle, unsigned long samples)
{
    runSynth(handle, samples, 0, 0);
}

void
SamplePlayer::deactivate(LADSPA_Handle handle)
{
    activate(handle); // both functions just reset the plugin
}

void
SamplePlayer::cleanup(LADSPA_Handle handle)
{
    delete (SamplePlayer*)handle;
}

void
SamplePlayer::update_dir(struct stat* info)
{
	// Get directory contents

	if (m_sampleSearchComplete) {
		m_sampleSearchComplete = false;
		searchSamples();
	}
	dir_stat_time = info->st_mtime;
}

char*
SamplePlayer::configure(LADSPA_Handle handle, const char* key, const char* value)
{
	SamplePlayer* player = (SamplePlayer *)handle;

	if (key && !strcmp(key, "sampledir")) {
		dbg(0, "SamplePlayer: sampledir: %s", value);

		player->lock();

		if(strcmp(value, player->m_sampleDir.c_str())){

			if (g_file_test(value, G_FILE_TEST_EXISTS) && g_file_test(value, G_FILE_TEST_IS_DIR)) {

				player->m_sampleDir = value;
				player->m_program = -1; // ensure tests for changed program are true

				struct stat info;
				stat(value, &info);

				player->update_dir(&info);

				player->unlock();
				return 0;

			} else {
				dbg(1, "SamplePlayer: dir doesnt exist");
				char* str = g_strdup_printf("Sample directory \"%s\" does not exist, leaving unchanged", value);
				player->unlock();
				return str;
			}
		}
		dbg(0, "dir unchanged.");

		struct stat info;
		stat(value, &info);
		time_t time = info.st_mtime;
		if(time > player->dir_stat_time){
			dbg(1, "directory modified. reloading...");
			player->update_dir(&info);
		}

		player->unlock();
		return 0;
	}

	player->unlock();
	return strdup("sampleplayer::configure: unknown configure key");
}

const DSSI_Program_Descriptor*
SamplePlayer::getProgram(LADSPA_Handle handle, unsigned long program)
{
	g_return_val_if_fail(handle, NULL);
	SamplePlayer* player = (SamplePlayer *)handle;

	if (!player->m_sampleSearchComplete) {
		player->lock();
		if (!player->m_sampleSearchComplete) {
			player->searchSamples();
		}
		player->unlock();
	}
	if (program >= player->m_samples.size()) return 0;

	static DSSI_Program_Descriptor descriptor;
	static char name[128];

	g_strlcpy(name, player->m_samples[program].first.c_str(), 128);

	descriptor.Bank = 0;
	descriptor.Program = program;
	descriptor.Name = name;

	return &descriptor;
}

void
SamplePlayer::selectProgram(LADSPA_Handle handle, unsigned long, unsigned long program)
{
	dbg(0, "*** PROGRAM_CHANGE *** %lu", program);
	SamplePlayer* player = (SamplePlayer*)handle;

	if (program < int(player->m_samples.size())) {
		std::string path = player->m_samples[program].second;
		std::string programName = player->m_samples[program].first;
		if (programName != player->m_program) {

			player->pending_program_change = program;

		} else {
			dbg(0, "program name unchanged");
			player->pending_rewind = true;
			return;
		}
	}
}

int
SamplePlayer::getMidiController(LADSPA_Handle, unsigned long port)
{
	int controllers[PortCount] = {
		DSSI_NONE,
		DSSI_NONE,
		DSSI_CC(12),
		DSSI_CC(13),
		DSSI_CC(64),
		DSSI_CC(72)
	};

	return controllers[port];
}

void
SamplePlayer::runSynth(LADSPA_Handle handle, unsigned long samples, snd_seq_event_t* events, unsigned long eventCount)
{
	((SamplePlayer*)handle)->runImpl(samples, events, eventCount);
}

void
SamplePlayer::receiveHostDescriptor(const DSSI_Host_Descriptor* descriptor)
{
	hostDescriptor = descriptor;
}

void
SamplePlayer::workThreadCallback(LADSPA_Handle handle)
{
	// non-realtime thread.
	// does disk io.
	// called by the host every 100ms

	//pchar(0, "W");

	SamplePlayer* player = (SamplePlayer*)handle;

#ifdef STREAMING
#ifdef USE_ADECODER
	if(player->pending_rewind && player->decoder.info.sample_rate){
#else
	if(player->pending_rewind && player->file){
#endif
		player->pending_rewind = false;
		player->buffer_read_pending = true;
		dbg(1, "seek");
#ifdef USE_ADECODER
		ad_seek(&player->decoder, 0);
#else
		sf_seek(player->file, 0, SEEK_SET);
#endif
		player->disk_read_complete = false;
		player->src_data.end_of_input = false;
		src_reset(player->src_state);
		player->clear_buffer();
	}
#endif

	if (player->pending_program_change >= 0) {

		dbg(0, "pending program change: %i", player->pending_program_change);

		player->lock();

		int program = player->pending_program_change;
		player->pending_program_change = -1;

		if (!player->m_sampleSearchComplete) {
			player->searchSamples();
		}

		if (program < int(player->m_samples.size())) {
			std::string path = player->m_samples[program].second;
			std::string programName = player->m_samples[program].first;
			if (programName != player->m_program) {
				player->m_program = programName;
				//g_mutex_unlock(player->mutex); //originally loadSampleData was not within the lock.
#ifdef STREAMING
				player->load_sample_data(path);

				if(!player->decoder.info.sample_rate){
					// loading failed
					player->buffer_read_pending = false;
					player->disk_read_complete = true;
					player->unlock();

					snd_seq_event_t event = {0,};
					hostDescriptor->midi_send(player, &event, 0); // send 'stopped'

					return;
				}
#else
				player->loadSampleData(path);
#endif
				player->unlock(); // loadSampleData is now within the lock.
			} else {
				dbg(0, "program name unchanged");
				player->unlock();
			}
		}
	}
#ifdef STREAMING
	else if(player->buffer_read_pending){
		player->buffer_read_pending = false;
		if(!player->disk_read_complete) player->fill_buffer();
	}
#endif

	// notify host of current playback position. Note player is polyphonic and we only send the position of the first sample.
	for (size_t i = 0; i < Polyphony; ++i) {
		if(player->ons[i] > -1){
			if(player->decoder.info.sample_rate){
				uint64_t t = (1000 * (player->sample_num - player->ons[i])) / player->decoder.info.sample_rate;
				snd_seq_event_t event = {0,};
				event.type = SND_SEQ_EVENT_SONGPOS;
				event.time.time = {(unsigned int)(t / 1000), (unsigned int)(1000 * (t % 1000))};
				event.data.raw32.d[0] = player->sample_num - player->ons[i];

				hostDescriptor->midi_send(handle, &event, 1);
			}
			break;
		}
	}

	if (!player->m_sampleSearchComplete) {

		player->lock();

		if (!player->m_sampleSearchComplete) {
			player->searchSamples();
		}

		player->unlock();
	}
}

void
SamplePlayer::searchSamples()
{
	if (m_sampleSearchComplete) return;

	m_samples.clear();

#ifdef DEBUG_SAMPLE_PLAYER
	dbg(0, "Directory is \"%s\"", m_sampleDir.c_str());
#endif

	GDir* dir = g_dir_open(m_sampleDir.c_str(), 0, NULL);
	if (dir) {
		const char* leaf;
		while ((leaf = g_dir_read_name(dir))) {
			if (leaf[0] == '.') continue;
			gchar* filename = g_build_filename(m_sampleDir.c_str(), leaf, NULL);
			if (!g_file_test(filename, G_FILE_TEST_IS_DIR)){
				m_samples.push_back(std::pair<std::string, std::string> (g_strdup(leaf), g_strdup(filename)));
				dbg(1, "  found: %s", leaf);
			}
			g_free(filename);
		}
		g_dir_close(dir);
	}
	else dbg(0, "cannot open directory %s", m_sampleDir.c_str());

	m_sampleSearchComplete = true;
}


#ifndef STREAMING
void
SamplePlayer::loadSampleData(std::string path)
{
	// Load the file into a buffer converted to the playback samplerate.

	// Mutex locking is needed to prevent the playback of the previous sample imediately following a program-select.
	// Why was only half of this locked?
	// -locking is currently done outside this fn.

	// 1- load whole file into a buffer.
	// 2- samplerate convert to playback rate into 2nd buffer.
	// 3- mix to output channel width.
	// 4- update class properties.

	dbg(0, "%s", g_strrstr(path.c_str(), "/") + 1);

#ifdef USE_ADECODER
	WfDecoder f = {{0,}};

	if(!ad_open(&f, path.c_str())) return false;

#else
	SF_INFO info;

	info.format = 0;
	SNDFILE* file = sf_open(path.c_str(), SFM_READ, &info);
	if (!file) {
		std::cerr << "SamplePlayer::loadSampleData: Failed to open audio file " << path.c_str() << ": " << sf_strerror(file) << std::endl;
		return;
	}
#endif

	size_t n_frames = info.frames;
	float* tmpFrames = (float*)malloc(info.frames * info.channels * sizeof(float));
	if (!tmpFrames) return;

	sf_readf_float(file, tmpFrames, info.frames);
	sf_close(file);

	if (info.samplerate != samplerate) {

		double ratio = (double)samplerate / (double)info.samplerate;
		size_t n_target_frames = (size_t)(info.frames * ratio);

		float* tmpResamples = (float*)g_malloc0(n_target_frames * info.channels * sizeof(float));
		if (!tmpResamples) {
			free(tmpFrames);
			return;
		}

		//TODO samplerate convert interleaved data? Check...
		SRC_DATA data;
		data.data_in = tmpFrames;
		data.data_out = tmpResamples;
		data.input_frames = info.frames;
		data.output_frames = n_target_frames;
		data.src_ratio = ratio;

		if (!src_simple(&data, SRC_SINC_BEST_QUALITY, info.channels)) {
			free(tmpFrames);
			tmpFrames = tmpResamples;
			n_frames = n_target_frames;
		} else {
			g_free(tmpResamples);
		}
	}

	/* add an extra sample for linear interpolation */
	float* tmp_samples = (float*)malloc((n_frames + 1) * sizeof(float));
	if (!tmp_samples) {
		free(tmpFrames);
		return;
	}

	// sum to mono. TODO update this to handle stereo.
	size_t i;
	for (i = 0; i < n_frames; ++i) {
		int j;
		tmp_samples[i] = 0.0f;
		for (j = 0; j < info.channels; ++j) {
			tmp_samples[i] += tmpFrames[i * info.channels + j];
		}
	}

	free(tmpFrames);

	/* add an extra sample for linear interpolation */
	tmp_samples[n_frames] = 0.0f;

//	g_mutex_lock(mutex);

	float* tmp_old = sample_data;
	sample_data = tmp_samples;
	m_sampleCount = n_frames;

	for (i = 0; i < Polyphony; ++i) {
		ons[i] = -1;
		offs[i] = -1;
		velocities[i] = 0;
	}

	if (tmp_old) free(tmp_old);

#if 0
	printf("%s: loaded %s (%ld samples from original %ld channels resampled from %ld frames at %ld Hz)\n", "sampler", path.c_str(), (long)n_frames, (long)info.channels, (long)info.frames, (long)info.samplerate);
#endif

//	g_mutex_unlock(mutex);
	dbg(0, "done");
}


#else
#define buffer_new(A) ({ \
	Buffer* buffer = (Buffer*)g_malloc0(A * sizeof(float) + 4 * sizeof(int)); \
	buffer->n_floats = A; \
	buffer->n_bytes = A * sizeof(float); \
	buffer->n_floats_per_channel = buffer->n_floats / decoder.info.channels; \
	buffer->n_bytes_per_channel = buffer->n_bytes / decoder.info.channels; \
	buffer; \
})


void
SamplePlayer::load_sample_data(std::string path)
{
#ifdef USE_ADECODER
	ad_clear_nfo(&decoder.info);
	ad_free_nfo(&decoder.info);
#else
	memset(&sf_info, 0, sizeof(SF_INFO));
#endif

	memset(&src_data, 0, sizeof(SRC_DATA));
	src_data.input_frames = BLOCK_N_FRAMES;
	disk_read_complete = false;

#ifdef USE_ADECODER
	if(decoder.info.sample_rate) // TODO
		ad_close(&decoder);
#else
	if(file){
		sf_close(file);
		file = 0;
	}
#endif

#ifdef USE_ADECODER
	if(!ad_open(&decoder, path.c_str())) return;

#else
	file = sf_open(path.c_str(), SFM_READ, &sf_info);
	if (!file) {
		dbg(0, "failed to open audio file %s: %s", path.c_str(), sf_strerror(file));
		return;
	}
#endif

#ifdef USE_ADECODER
	src_data.src_ratio = (decoder.info.sample_rate == samplerate) ? 1.0 : (double)samplerate / (double)decoder.info.sample_rate;
#else
	src_data.src_ratio = (sf_info.samplerate == samplerate) ? 1.0 : (double)samplerate / (double)sf_info.samplerate;
#endif
	dbg(1, "src_ratio=%.4f", src_data.src_ratio);

#ifdef USE_ADECODER
	if(decoder.info.frames * src_data.src_ratio < BLOCK_N_FRAMES){
#else
	if(sf_info.frames * src_data.src_ratio < BLOCK_N_FRAMES){
#endif
		dbg(0, "single block sample");
	}

	if(src_state) src_state = src_delete (src_state);
	int error;
#ifdef USE_ADECODER
	if((src_state = src_new (SRC_SINC_BEST_QUALITY, decoder.info.channels, &error)) == NULL){
#else
	if((src_state = src_new (SRC_SINC_BEST_QUALITY, sf_info.channels, &error)) == NULL){
#endif
		dbg(0, "\n\nError : src_new() failed : %s.\n", src_strerror(error));
		return;
	};

	if(rb[0]){
		jack_ringbuffer_reset(rb[0]);
		jack_ringbuffer_reset(rb[1]);
		fill_buffer();
	}else{
		const int rb_size = 128 * 1024;
		if((rb[0] = jack_ringbuffer_create(rb_size)) && (rb[1] = jack_ringbuffer_create(rb_size))){
			dbg(2, "ringbuffer created ok.");
			if(buffer) g_free(buffer);
			buffer = buffer_new(BLOCK_N_FRAMES * decoder.info.channels);

			fill_buffer();
		}else{
			dbg(0, "ringbuffer create error.");
		}

	}
}
#endif // STREAMING


void
SamplePlayer::fill_buffer()
{
	#define SRC_EXTRA 8
#ifdef USE_ADECODER
	g_return_if_fail(decoder.info.sample_rate);
	g_return_if_fail(decoder.info.channels);
#else
	g_return_if_fail(file);
#endif
	pchar(2, "F");

#ifdef USE_ADECODER
	int bytes_to_read = BLOCK_N_FRAMES * decoder.info.channels * sizeof(float);
#else
	int bytes_to_read = BLOCK_N_FRAMES * sf_info.channels * sizeof(float);
#endif

	const int max_channels = 2;
	float* ch_buf[max_channels];
	#define BARRIER 256 //TODO find why this is neccesary (eg playing a stereo file with src from 44.1 to 48).
#ifdef USE_ADECODER
	ch_buf[0] = decoder.info.channels > 1 ? (float*)g_malloc0(buffer->n_bytes_per_channel * src_data.src_ratio + BARRIER) : NULL;
	ch_buf[1] = decoder.info.channels > 1 ? (float*)g_malloc0(buffer->n_bytes_per_channel * src_data.src_ratio + BARRIER) : NULL;
#else
	ch_buf[0] = sf_info.channels > 1 ? (float*)g_malloc0(buffer->n_bytes_per_channel * src_data.src_ratio + BARRIER) : NULL;
	ch_buf[1] = sf_info.channels > 1 ? (float*)g_malloc0(buffer->n_bytes_per_channel * src_data.src_ratio + BARRIER) : NULL;
#endif

	size_t src_out_n_frames = (size_t)BLOCK_N_FRAMES;
	float* src_out = NULL;
#ifdef USE_ADECODER
	if (decoder.info.sample_rate != samplerate) {
#else
	if (sf_info.samplerate != samplerate) {
#endif
		src_out_n_frames = (size_t)(BLOCK_N_FRAMES * src_data.src_ratio + SRC_EXTRA);

#ifdef USE_ADECODER
		if(!(src_out = (float*)g_malloc0(src_out_n_frames * decoder.info.channels * sizeof(float)))){
#else
		if(!(src_out = (float*)g_malloc0(src_out_n_frames * sf_info.channels * sizeof(float)))){
#endif
			return;
		}
	}
	int debug_loop_max = 48; int i = 0;
	while(jack_ringbuffer_write_space(rb[0]) > bytes_to_read * src_data.src_ratio){

#ifdef USE_ADECODER
		sf_count_t frames_read = ad_read(&decoder, &buffer->data, buffer->n_floats) / decoder.info.channels;
#else
		sf_count_t frames_read = sf_readf_float(file, &buffer->data, BLOCK_N_FRAMES);
#endif
#ifdef USE_ADECODER
		int bytes_read = frames_read * decoder.info.channels * sizeof(float);
#else
		int bytes_read = frames_read * sf_info.channels * sizeof(float);
#endif
		src_data.end_of_input = (frames_read < BLOCK_N_FRAMES);
		if(src_data.end_of_input){
			dbg(1, "EOF");
			disk_read_complete = true;
		}

		src_out_n_frames = (size_t)(frames_read * src_data.src_ratio);
//dbg(0, "%i src_out_n_frames=%lu %lu", sizeof(size_t), src_out_n_frames, frames_read);
#ifdef USE_ADECODER
		int bytes_to_write = frames_read * src_data.src_ratio * decoder.info.channels * sizeof(float);
#else
		int bytes_to_write = frames_read * src_data.src_ratio * sf_info.channels * sizeof(float);
#endif
		int bytes_to_write_per_channel = frames_read * src_data.src_ratio * sizeof(float);

#ifdef USE_ADECODER
		if (decoder.info.sample_rate != samplerate) {
#else
		if (sf_info.samplerate != samplerate) {
#endif
			//pchar(0, "C");
			src_out_n_frames += SRC_EXTRA;

			src_data.data_in = &buffer->data;
			src_data.input_frames = frames_read;
			src_data.data_out = src_out;
			src_data.output_frames = src_out_n_frames;

			int error;
			if ((error = src_process (src_state, &src_data))){
				dbg(0, "\nError : %s", src_strerror (error));
				break;
			};
			dbg(2, "src frames_read=%li frames_used=%li generated=%li/%li", src_data.input_frames, src_data.input_frames_used, src_data.output_frames_gen, src_data.output_frames);

			bytes_to_write_per_channel = src_data.output_frames_gen * sizeof(float);
		}else{
			src_out = &buffer->data;
		}

		//de-interleave:
#ifdef USE_ADECODER
		if(decoder.info.channels == 1){
#else
		if(sf_info.channels == 1){
#endif
			ch_buf[0] = src_out;
			ch_buf[1] = src_out;
		}else{
			for(int f = 0; f < src_out_n_frames; f++){
				float* a = ch_buf[0] + f;
#ifdef USE_ADECODER
				*(ch_buf[0] + f) = *(src_out + f * decoder.info.channels);
#else
				*a = *(src_out + f * sf_info.channels);
#endif
				float* b = ch_buf[1] + f;
#ifdef USE_ADECODER
				*b = *(src_out + f * decoder.info.channels + 1);
#else
				*b = *(src_out + f * sf_info.channels + 1);
#endif
			}
		}

		for(int ch=0;ch<2;ch++){
			if(jack_ringbuffer_write(rb[ch], (char*)ch_buf[ch], bytes_to_write_per_channel) < bytes_to_write_per_channel){
				dbg(0, "ringbuffer write error. ch=%i n_bytes=%i", ch, bytes_to_write);
			}
		}
#ifdef DEBUG_SAMPLE_PLAYER
		total_written += (bytes_to_write_per_channel / sizeof(float));
#endif

		if(frames_read < BLOCK_N_FRAMES) break;
		if(i++ > debug_loop_max){
			pchar(0, "!");
			break;
		}
	}
#ifdef USE_ADECODER
	if(decoder.info.channels > 1) for(int c=0;c<max_channels;c++) if(c < decoder.info.channels) g_free(ch_buf[c]);
#else
	if(sf_info.channels > 1) for(int c=0;c<max_channels;c++) if(c < sf_info.channels) g_free(ch_buf[c]);
#endif
#ifdef USE_ADECODER
	if (decoder.info.sample_rate != samplerate) g_free(src_out);
#else
	if (sf_info.samplerate != samplerate) g_free(src_out);
#endif
}


void
SamplePlayer::clear_buffer()
{
	for(int ch=0;ch<2;ch++){
		jack_ringbuffer_reset(rb[ch]);
	}
}


void
SamplePlayer::process_event(snd_seq_event_t* event)
{
	if (event->type == SND_SEQ_EVENT_NOTEON) {
#ifdef DEBUG_SAMPLE_PLAYER
		dbg(0, "NOTEON at time %i", event->time.tick);
#endif
		snd_seq_ev_note_t n = event->data.note;
		if (n.velocity > 0) {
			dbg(1, "index=%i\n", n.note);
			ons[n.note] = sample_num + event->time.tick;
			offs[n.note] = -1;
			velocities[n.note] = n.velocity;
#ifdef STREAMING
			//maybe cleaner if we seek only at note off?
			if(!pending_program_change) pending_rewind = true;
#endif
		} else {
			if (!sustain || (*sustain < 0.001)) {
				offs[n.note] = sample_num + event->time.tick;
			}
		}
	} else if (event->type == SND_SEQ_EVENT_NOTEOFF && (!sustain || (*sustain < 0.001))) {
#ifdef DEBUG_SAMPLE_PLAYER
		dbg(0, "NOTEOFF at time %i", event->time.tick);
#endif
		snd_seq_ev_note_t n = event->data.note;
		dbg(0, "index=%i", n.note);
		offs[n.note] = sample_num + event->time.tick;

		pending_rewind = true;
	}
	else dbg(0, "unknown event type.");
}


void
SamplePlayer::runImpl(unsigned long sampleCount, snd_seq_event_t* events, unsigned long eventCount)
{
	memset(output1, 0, sampleCount * sizeof(float));
	memset(output2, 0, sampleCount * sizeof(float));

	bool debug_deferred = false;
	if(eventCount && ((
		#ifndef STREAMING
			!sample_data ||
		#endif
#ifdef USE_ADECODER
			!decoder.info.frames) || pending_program_change >= 0 || pending_rewind)){
#else
			!sf_info.frames) || pending_program_change >= 0 || pending_rewind)){
#endif
		//printf("cannot play note before sample loaded.\n");
		unsigned long event_pos;
		for (event_pos = 0; event_pos < eventCount; event_pos++) {
			deferred_events.push_back(events[event_pos]);
		}
		debug_deferred = true;
	}

	if(!g_mutex_trylock(&mutex)){
		//pchar(0, "X");
		return;
	}

	if (
		#ifndef STREAMING
			!sample_data ||
		#endif
#ifdef USE_ADECODER
			!decoder.info.frames) {
#else
			!sf_info.frames) {
#endif
		//pchar(0, "0");
		sample_num += sampleCount;

		unlock();
		return;
	}

	if(eventCount) dbg(1, "event count=%lu", eventCount);
	if(pending_program_change < 0 && !pending_rewind){
		//pchar(0, ".");
		if(debug_deferred) dbg(0, "\n!!!!!error!!!!! should not get here.\n");
		if(deferred_events.size()){
			dbg(1, "n_deferred=%i", deferred_events.size());

			std::vector<snd_seq_event_t>::iterator x = deferred_events.begin();
			for ( ; x != deferred_events.end(); ++x) {
				dbg(1, "processing deferred event...");
				process_event(&(*x));
			}
			deferred_events.clear();
		}

		unsigned long pos;
		unsigned long event_pos;
		for (pos = 0, event_pos = 0; pos < sampleCount; ) {

			while (event_pos < eventCount && pos >= events[event_pos].time.tick) {

				process_event(&events[event_pos]);

				++event_pos;
			}

			unsigned long count = sampleCount - pos;
			if (event_pos < eventCount &&
				events[event_pos].time.tick < sampleCount) {
				count = events[event_pos].time.tick - pos;
			}

			int notecount = 0;

			for (int i = 0; i < Polyphony; ++i) {
				if (ons[i] >= 0) {
					++notecount;
#ifdef STREAMING
					feed_ringbuffer(i, pos, count);
#else
					addSample(i, pos, count);
#endif
				}
			}

			#if 0
			#ifdef DEBUG_SAMPLE_PLAYER
			std::cerr << "SamplePlayer: have " << notecount << " note(s) sounding currently" << std::endl;
			#endif
			#endif

			pos += count;
		}
#ifndef STREAMING
		memcpy(output2, output1, sampleCount * sizeof(float)); //TODO
#endif
	}

	sample_num += sampleCount;
	unlock();
}


#ifdef STREAMING
void
SamplePlayer::feed_ringbuffer(int n, unsigned long pos, unsigned long count)
{
	//pchar(0, "A", n);

	if (offs[n] >= 0) {
		ons[n] = -1; //TODO release

		return;
	}

	unsigned s = pos + sample_num - ons[n];
#ifdef USE_ADECODER
	if(s > decoder.info.frames * src_data.src_ratio){
#else
	if(s > sf_info.frames * src_data.src_ratio){
#endif

		dbg(1, "note finished? %li", sample_num);
		ons[n] = -1;
		//////////////////////
		snd_seq_event_t event = {0,};
		hostDescriptor->midi_send(this, &event, 0);
		//////////////////////
	}

	size_t read_available = jack_ringbuffer_read_space(rb[0]);

	if(read_available < count){
		dbg(1, "reduced read");
	}

	int bytes = MIN(count, read_available) * sizeof(float);
	//copy from the ringbuffer into the jack buffer:
	size_t read =
	jack_ringbuffer_read(rb[0], (char*)output1, bytes); // the count is in *bytes*.
	jack_ringbuffer_read(rb[1], (char*)output2, bytes);
	if(read < bytes){
		dbg(1, "short read");
	}
#ifdef DEBUG_SAMPLE_PLAYER
#ifdef USE_ADECODER
	float max = decoder.info.frames * src_data.src_ratio;
#else
	float max = sf_info.frames * src_data.src_ratio;
#endif
	total_played += (read /sizeof(float));
	//dbg(0, "total: w:%u r:%i max=%Lu/%.1f", total_written, total_played, sf_info.frames, max);
#endif

	buffer_read_pending = true;
}


#else
void
SamplePlayer::addSample(int n, unsigned long pos, unsigned long count)
{
	// Render a chunk of audio into the output buffer.

	float ratio = 1.f;
	if (retune && *retune) {
		if (concert_a) {
			ratio *= *concert_a / 440.f;
		}
		if (base_pitch && n != *base_pitch) {
			ratio *= powf(1.059463094f, n - *base_pitch);
		}
	}

	if (long(pos + sample_num) < ons[n]) return;

	float gain = (float)velocities[n] / 127.0f;

	unsigned long i, s;
	for (i = 0, s = pos + sample_num - ons[n]; i < count; ++i, ++s) {

		float         lgain = gain;
		float         rs = s * ratio;
		unsigned long rsi = lrintf(floor(rs));

		if (rsi >= m_sampleCount) {
#ifdef DEBUG_SAMPLE_PLAYER
			std::cerr << "Note " << n << " has run out of samples (were " << m_sampleCount << " available at ratio " << ratio << "), ending" << std::endl;
#endif
			ons[n] = -1;
			//////////////////////
			snd_seq_event_t event = {0,};
			hostDescriptor->midi_send(this, &event, NULL);
			//////////////////////
			break;
		}

		if (offs[n] >= 0 && long(pos + i + sample_num) > offs[n]) {

			unsigned long dist = pos + i + sample_num - offs[n];

			unsigned long releaseFrames = 200;
			if (m_release) {
				releaseFrames = long(*m_release * samplerate + 0.0001);
			}

			if (dist > releaseFrames) {
#ifdef DEBUG_SAMPLE_PLAYER
				std::cerr << "Note " << n << " has expired its release time (" << releaseFrames << " frames), ending" << std::endl;
#endif
				ons[n] = -1;
				break;
			} else {
				lgain = lgain * (float)(releaseFrames - dist) / (float)releaseFrames;
			}
		}

		float sample = sample_data[rsi] + ((sample_data[rsi + 1] - sample_data[rsi]) * (rs - (float)rsi));

		output1[pos + i] += lgain * sample;
	}
}
#endif //STREAMING


void
SamplePlayer::lock()
{
	dbg(2, "... %p", &mutex);

	#ifdef XDEBUG_SAMPLE_PLAYER
	if(g_mutex_trylock(&mutex)) g_mutex_unlock(&mutex);
	else dbg(0, "already locked");
	#endif

	g_mutex_lock(&mutex);
}


void
SamplePlayer::unlock()
{
	g_mutex_unlock(&mutex);
}


static void
_output(const char* str, gboolean newline)
{
	FILE* logfile = prefer_stdout ? NULL : fopen (LOG_DIR "/auditioner.log", "a"); 
	FILE* stream = logfile ? logfile : stderr;
	fprintf(stream, "%s", str);

	if(newline){
		fprintf(stream, "\n");
	}else{
		fflush(stream);
	}

	if(logfile) fclose (logfile);
}


void
SamplePlayer::debug_printf(const char* class_, const char* func, int level, const char* format, ...)
{
	va_list args;

	va_start(args, format);
	if (level <= debug) {
#if 0
		FILE* logfile = fopen (LOG_DIR "/auditioner.log", "a"); 
		FILE* out = logfile ? logfile : stderr;
		fprintf(out, "%s::%s(): ", class_, func);
		vfprintf(out, format, args);
		fprintf(out, "\n");

		if(logfile) fclose (logfile);
#else
		gchar* s = g_strdup_vprintf(format, args);
		gchar* t = g_strdup_printf("%s(): %s", func, s);
		_output(t, true);
		g_free(s);
		g_free(t);
#endif
	}
	va_end(args);
}


void
pchar(int level, const char* format, ...)
{
	// Has no newline added.

	va_list argp;
	va_start(argp, format);
    if (level <= debug) {
		gchar* s = g_strdup_vprintf(format, argp);
		_output(s, false);
		g_free(s);
	}
	va_end(argp);
}


