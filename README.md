
Ayyi Auditioner
===============

The Ayyi Auditioner is a service that is connected to Jack[1] and can
playback audio samples triggered via Alsa Midi and Dbus.

## Why?

 - Ayyi Auditioner gives the user a dedicated channel that can be set up and
   optimised (eg, routing and fx) for the purpose of auditioning from multiple
   applications in a modular setup.

 - Free's developers from having to independently re-implement the same
   auditioning functionality, i.e. promotes code re-use and encourages
   best-of-breed. Allows audio applications to reduce runtime resources
   and concentrate on their core functionality.


## Features

 - Resampling: the sample can be played at different pitches and is resampled
   to the jackd sample rate.

 - Midi triggering. Once a directory has been selected via Dbus, the current
   sample can be played at different pitches across the keyboard. Samples in
   the current directory can be selected using Midi program change.


## Usage

 - The auditioner service is intended to be used with a supporting application such
   as [Samplecat](ayyi.github.io/samplecat/).

 - An example command line client program is included. Running the 'audition'
   command with a filename argument will request the auditioner service to
   play the given file.

 - Developers mostly need to use the Dbus Api. Essentially this consists of
   calling `auditioner.playback_start (sample_path)`


## Installation

 - Ayyi auditioner is developed on GNU/Linux. It may also work on other systems.

 - Dependencies include dbus-glib, ladspa, jack-audio-connection-kit, libsamplerate,
   alsa, ffmpeg, libsndfile. A list of packages for these dependencies can be
   found for some distributions in the Docker files in the test directory.

 - ``./autogen.sh && ./configure && make``  
   If installing from a tarball you can omit the autogen step.

 - To use the auditioner without installing, you can either launch the service
   manually or copy `auditioner.service` to `/usr/share/dbus-1/services/`


## Limitations

 - Only mono and stereo are supported.
 - Playback at different pitches is temporarily disabled.


Ayyi Auditioner uses code from the Sonic Visualizer and DSSI projects. Thanks
to Chris Cannam and others.


[1] [JACK Audio Connection Kit](http://www.jackaudio.org/)
