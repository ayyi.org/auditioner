/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include <stdbool.h>

#define dbg(A, B, ...) debug_printf(__func__, A, B, ##__VA_ARGS__)
#define perr(A, ...) g_critical("%s(): "A, __func__, ##__VA_ARGS__)
#define pwarn(A, ...) g_warning("%s(): "A, __func__, ##__VA_ARGS__);

#ifndef true
#define true TRUE
#define false FALSE
#endif

#ifdef __utils_c__
char ok       [32] = " [ \x1b[1;32mok\x1b[0;39m ]";
char fail     [32] = " [\x1b[1;31mFAIL\x1b[0;39m]";
char ayyi_warn[32] = "\x1b[1;33mwarning:\x1b[0;39m";
char ayyi_err [32] = "\x1b[1;31merror!\x1b[0;39m";
char white    [16] = "\x1b[0;39m"; // 0 = normal
char green_r  [16] = "\x1b[30;42m";
char blue_r   [16] = "\x1b[34;42m";
#else
extern int debug;

extern char ayyi_warn[32];
extern char ayyi_err [32];
extern char ok       [];
extern char fail     [];
extern char white    [];
extern char green_r  [];
extern char blue_r   [];
#endif

extern bool prefer_stdout;

void        debug_printf (const char* func, int level, const char* format, ...);
void        errprintf    (const char* fmt, ...);
void        p            (int level, const char* format, ...);
void        pchar        (int level, const char* format, ...);
void        clear_log    ();

void       filename_get_extension    (const char* path, char* extn);
