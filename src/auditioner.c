/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2011-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  auditioner.c
 |
 |  In the sampler plugin, when a new NOTE_ON arrives, the following occurs:
 |
 |    1. run() trys mutex and checks for new midi events
 |    2. if pending program change, or no sample loaded, push event to deferred queue.
 |    3. when program change is finished, the alsa event is put into ons/offs/velocities.
 |
 |  Playback has to wait for 2 things:
 |
 |    1. pending program change
 |    2. mutex free - locked by worker thread when doing sample-load.
 |
 +----------------------------------------------------------------------
 |
 |  based on jack-dssi-host.c
 |
 |  Copyright 2004, 2009 Chris Cannam, Steve Harris and Sean Bolton.
 |
 |  Permission to use, copy, modify, distribute, and sell this software
 |  for any purpose is hereby granted without fee, provided that the
 |  above copyright notice and this permission notice are included in
 |  all copies or substantial portions of the software.
 |
 */

#define _DEFAULT_SOURCE 1
#define _SVID_SOURCE    1
#define _ISOC99_SOURCE  1

#include <config.h>
#include <stdio.h>
#include <getopt.h>
#include <dlfcn.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <glib.h>
#include <ladspa.h>
#include "plugin/api/dssi.h"
#include <alsa/asoundlib.h>
#include <alsa/seq.h>
#include <jack/jack.h>
#include <libgen.h>
#ifdef USE_OSC
#include <lo/lo.h>
#endif

#include "dbus.h"
#include "utils.h"
#include "auditioner.h"
#include "message_buffer.h"

static const struct option long_options[] = {
  { "verbose",         1, NULL, 'v' },
  { "help",            0, NULL, 'h' },
  { "no-autoconnect",  0, NULL, 'a' },
};

static const char* const short_options = "v:ha";

static const char* const usage =
  "Usage: %s [ options ]\n"
  " -a --no-autoconnect dont connect jack outputs.\n"
  " -v --verbose        show more information (0-2).\n"
  " -h --help           show this usage information and quit.\n"
  "\n";


#ifdef DBUS_HAS_OWN_THREAD
GAsyncQueue* msg_queue = NULL;
#endif

GMainLoop*               main_loop;

static GList*            play_queue = NULL;

#define MIDI_ALSA //TODO
#ifdef MIDI_ALSA
static snd_seq_t*        alsa_client;
#endif

static jack_client_t*    jackClient;
static jack_port_t**     inputPorts, **outputPorts;

static d3h_dll_t*        dlls;

static d3h_plugin_t*     plugins;
static int               plugin_count = 0;

static float             sample_rate;

static d3h_instance_t    instances[D3H_MAX_INSTANCES];
static int               instance_count = 0;

static LADSPA_Handle*    instanceHandles;
static snd_seq_event_t** instanceEventBuffers;
static unsigned long*    instanceEventCounts;

static int               insTotal, outsTotal;
static float**           pluginInputBuffers, **pluginOutputBuffers;

static int               controlInsTotal, controlOutsTotal;
static float*            pluginControlIns, *pluginControlOuts;
static d3h_instance_t*   channel2instance[D3H_MAX_CHANNELS]; /* maps MIDI channel to instance */
static d3h_instance_t**  pluginControlInInstances;           /* maps global control in # to instance */
static unsigned long*    pluginControlInPortNumbers;         /* maps global control in # to instance LADSPA port # */
static int*              pluginPortUpdated;                  /* indexed by global control in # */

static char*             projectDirectory;

static sigset_t          _signals;

int exiting = 0;
gboolean playing = false;
//static int verbose = 0;
static gboolean autoconnect = true;
const char* myName = NULL;

#define EVENT_BUFFER_SIZE 1024
static snd_seq_event_t midiEventBuffer[EVENT_BUFFER_SIZE]; /* ring buffer */
static int midiEventReadIndex = 0, midiEventWriteIndex = 0;

static pthread_mutex_t midiEventBufferMutex = PTHREAD_MUTEX_INITIALIZER;

static void start_dbus          ();
LADSPA_Data get_port_default    (const LADSPA_Descriptor* plugin, int port);

#ifdef USE_OSC
static char              osc_path_tmp[1024];
lo_server_thread         serverThread;

static void osc_error           (int num, const char* m, const char* path);
extern int  osc_message_handler (const char* path, const char* types, lo_arg**, int argc, void* data, void* user_data);
extern int  osc_debug_handler   (const char* path, const char* types, lo_arg**, int argc, void* data, void* user_data);
#endif


int
request_non_rt_thread (LADSPA_Handle instance, void (*RunFunction)(LADSPA_Handle Instance))
{
	NonRTPluginThread* thread = g_new0(NonRTPluginThread, 1);
	thread->handle = instance;
	thread->runFunction = RunFunction;

	void run_thread(NonRTPluginThread* thread)
	{
		while (!exiting) {
			thread->runFunction(thread->handle);
			usleep(100000);
		}
	}

	if(!g_thread_new("", (GThreadFunc)run_thread, thread)){
		dbg(0, "error creating thread");
	}

	return 0;
}


	gboolean next (gpointer user_data)
	{
		dbg(0, "queue len: %i", g_list_length(play_queue));

		char* sample = play_queue->data;
		GList* next = play_queue;
		play_queue = g_list_remove_link(play_queue, play_queue);
		play_note(sample, NULL);

		g_list_free(next);
		g_free(sample);

		return G_SOURCE_REMOVE;
	}

void
midi_send (LADSPA_Handle instance, snd_seq_event_t* event, unsigned long event_count)
{
	g_return_if_fail(event);

	switch(event->type){
		case SND_SEQ_EVENT_SONGPOS:
			;double seconds = (double)event->time.time.tv_sec + ((double)event->time.time.tv_nsec) / 1000000.0;
			auditioner_dbus_emit_position(auditioner_dbus_get_instance(), event->data.raw32.d[0], seconds, NULL);
			break;

		default:
			if(!event_count){
				dbg(0, "-------------------------------------");
				auditioner_dbus_emit_stopped(auditioner_dbus_get_instance(), NULL);

				if(play_queue){
					g_idle_add(next, NULL);
				}
				else playing = false;
			}
	}
}


DSSI_Host_Descriptor host_descriptor = {
	2,
	NULL,      // request_transport_information
	NULL,      // request_midi_send
	midi_send,
	request_non_rt_thread
};


void
signalHandler (int sig)
{
	dbg(0, "signal %d caught. exiting...", sig);
	g_main_loop_quit(main_loop);
	exiting = 1;
}


gboolean
midi_callback (gpointer user_data)
{
	pthread_mutex_lock(&midiEventBufferMutex);

#ifdef MIDI_ALSA
	snd_seq_event_t *ev = 0;
	struct timeval tv;

	/* If events remain on the input buffer of user-space, function returns the total byte size of events on it. If fetch_sequencer argument is non-zero, this function checks the presence of events on sequencer FIFO When events exist, they are transferred to the input buffer, and the number of received events are returned. If fetch_sequencer argument is zero and no events remain on the input buffer, function simply returns zero. */
	//printf("pending=%i\n", snd_seq_event_input_pending(alsa_client, 0)); //always zero

	do {
		/* snd_seq_event_input

		   Obtains an input event from sequencer. The event is created via snd_seq_create_event(),
		   and its pointer is stored on ev argument.

		   This function firstly receives the event byte-stream data from sequencer as much as possible at once.
		   Then it retrieves the first event record and store the pointer on ev. By calling this function sequentially,
		   events are extracted from the input buffer.

		   If there is no input from sequencer, function falls into sleep in blocking mode until an event is received,
		   or returns -EAGAIN error in non-blocking mode. Occasionally, this function may return -ENOSPC error.
		   This means that the input FIFO of sequencer overran, and some events are lost. Once this error is returned,
		   the input FIFO is cleared automatically.

		   Function returns the byte size of remaining events on the input buffer if an event is successfully received. Application can determine from the returned value whether to call input once more or not. */
		if (snd_seq_event_input(alsa_client, &ev) > 0) {

			if (midiEventReadIndex == midiEventWriteIndex + 1) {
				dbg(0, "%s: Warning: MIDI event buffer overflow! ignoring incoming event", myName);
				continue;
			}
			dbg(0, "host: midi event!");

			midiEventBuffer[midiEventWriteIndex] = *ev;

			ev = &midiEventBuffer[midiEventWriteIndex];

			/* At this point we change the event timestamp so that its
			   real-time field contains the actual time at which it was
			   received and processed (i.e. now).  Then in the audio
			   callback we use that to calculate frame offset. */

			gettimeofday(&tv, NULL);
			ev->time.time.tv_sec = tv.tv_sec;
			ev->time.time.tv_nsec = tv.tv_usec * 1000L;

			if (ev->type == SND_SEQ_EVENT_NOTEON && ev->data.note.velocity == 0) {
				ev->type =  SND_SEQ_EVENT_NOTEOFF;
			}

			/* We don't need to handle EVENT_NOTE here, because ALSA
			   won't ever deliver them on the sequencer queue -- it
			   unbundles them into NOTE_ON and NOTE_OFF when they're
			   dispatched.  We would only need worry about them when
			   retrieving MIDI events from some other source. */

			midiEventWriteIndex = (midiEventWriteIndex + 1) % EVENT_BUFFER_SIZE;
		}

	} while (snd_seq_event_input_pending(alsa_client, 0) > 0);
#endif

	pthread_mutex_unlock(&midiEventBufferMutex);

	return G_SOURCE_CONTINUE;
}


void
setControl (d3h_instance_t* instance, long controlIn, snd_seq_event_t *event)
{
	long port = pluginControlInPortNumbers[controlIn];

	const LADSPA_Descriptor* p = instance->plugin->descriptor->LADSPA_Plugin;

	LADSPA_PortRangeHintDescriptor d = p->PortRangeHints[port].HintDescriptor;

	LADSPA_Data lb = p->PortRangeHints[port].LowerBound * (LADSPA_IS_HINT_SAMPLE_RATE(p->PortRangeHints[port].HintDescriptor) ?  sample_rate : 1.0f);
	LADSPA_Data ub = p->PortRangeHints[port].UpperBound * (LADSPA_IS_HINT_SAMPLE_RATE(p->PortRangeHints[port].HintDescriptor) ?  sample_rate : 1.0f);

	float value = (float)event->data.control.value;

	if (!LADSPA_IS_HINT_BOUNDED_BELOW(d)) {
		if (!LADSPA_IS_HINT_BOUNDED_ABOVE(d)) {
			/* unbounded: might as well leave the value alone. */
			return;
		} else {
			/* bounded above only. just shift the range. */
			value = ub - 127.0f + value;
		}
	} else {
		if (!LADSPA_IS_HINT_BOUNDED_ABOVE(d)) {
			/* bounded below only. just shift the range. */
			value = lb + value;
		} else {
			/* bounded both ends.  more interesting. */
			if (LADSPA_IS_HINT_LOGARITHMIC(d) && lb > 0.0f && ub > 0.0f) {
				const float llb = logf(lb);
				const float lub = logf(ub);

				value = expf(llb + ((lub - llb) * value / 127.0f));
			} else {
				value = lb + ((ub - lb) * value / 127.0f);
			}
		}
	}
	if (LADSPA_IS_HINT_INTEGER(d)) {
		value = lrintf(value);
	}

	if (debug) {
		dbg(0, "%s: %s MIDI controller %d=%d -> control in %ld=%f", myName,
		instance->friendly_name, event->data.control.param,
		event->data.control.value, controlIn, value);
	}

	pluginControlIns[controlIn] = value;
	pluginPortUpdated[controlIn] = 1;
}


int
audio_callback (jack_nframes_t nframes, void *arg)
{
	int i;
	int outCount, inCount;
	struct timeval tv, evtv, diff;

	gettimeofday(&tv, NULL);

	/* Not especially pretty or efficient */

	for (i = 0; i < instance_count; i++) {
		instanceEventCounts[i] = 0;
	}

	long framediff;
	for ( ; midiEventReadIndex != midiEventWriteIndex; midiEventReadIndex = (midiEventReadIndex + 1) % EVENT_BUFFER_SIZE) {

		snd_seq_event_t* ev = &midiEventBuffer[midiEventReadIndex];

		if (!snd_seq_ev_is_channel_type(ev)) {
			/* discard non-channel oriented messages */
			continue;
		}

		d3h_instance_t* instance = channel2instance[ev->data.note.channel];
		if (!instance
			/* || instance->inactive */) /* no -- see comment in osc_exiting_handler */
		{
			/* discard messages intended for channels we aren't using or absent or exited plugins */
			continue;
		}
		i = instance->number;

		/* Stop processing incoming MIDI if an instance's event buffer is
		 * full. */
		if (instanceEventCounts[i] == EVENT_BUFFER_SIZE) break;

		/* Each event has a real-time timestamp indicating when it was
		 * received (set by midi_callback).  We need to calculate the
		 * difference between then and the start of the audio callback
		 * (held in tv), and use that to assign a frame offset, to
		 * avoid jitter.  We should stop processing when we reach any
		 * event received after the start of the audio callback. */

		evtv.tv_sec = ev->time.time.tv_sec;
		evtv.tv_usec = ev->time.time.tv_nsec / 1000;

		if (evtv.tv_sec > tv.tv_sec || (evtv.tv_sec == tv.tv_sec && evtv.tv_usec > tv.tv_usec)) {
			break;
		}

		diff.tv_sec = tv.tv_sec - evtv.tv_sec;
		if (tv.tv_usec < evtv.tv_usec) {
			--diff.tv_sec;
			diff.tv_usec = tv.tv_usec + 1000000 - evtv.tv_usec;
		} else {
			diff.tv_usec = tv.tv_usec - evtv.tv_usec;
		}

		framediff =
			diff.tv_sec * sample_rate +
			((diff.tv_usec / 1000) * sample_rate) / 1000 +
			((diff.tv_usec - 1000 * (diff.tv_usec / 1000)) * sample_rate) / 1000000;

		if (framediff >= nframes) framediff = nframes - 1;
		else if (framediff < 0) framediff = 0;

		ev->time.tick = nframes - framediff - 1;

		if (ev->type == SND_SEQ_EVENT_CONTROLLER) {
	    
			int controller = ev->data.control.param;
#ifdef DEBUG
			MB_MESSAGE("%s CC %d(0x%02x) = %d\n", instance->friendly_name, controller, controller, ev->data.control.value);
#endif

			if (controller == 0) { // bank select MSB

				instance->pendingBankMSB = ev->data.control.value;

			} else if (controller == 32) { // bank select LSB

				instance->pendingBankLSB = ev->data.control.value;

			} else if (controller > 0 && controller < MIDI_CONTROLLER_COUNT) {

				long controlIn = instance->controllerMap[controller];
				if (controlIn >= 0) {

					/* controller is mapped to LADSPA port, update the port */
					setControl(instance, controlIn, ev);

				} else {

					/* controller is not mapped, so pass the event through to plugin */
					instanceEventBuffers[i][instanceEventCounts[i]] = *ev;
					instanceEventCounts[i]++;
				}
			}

		} else if (ev->type == SND_SEQ_EVENT_PGMCHANGE) {

			instance->pendingProgramChange = ev->data.control.value;

		} else {

			instanceEventBuffers[i][instanceEventCounts[i]] = *ev;
			instanceEventCounts[i]++;
		}
	}

	/* process pending program changes */
	for (i = 0; i < instance_count; i++) {
		d3h_instance_t* instance = &instances[i];

		/* no -- see comment in osc_exiting_handler */
		/* if (instance->inactive) continue; */

		if (instance->pendingProgramChange >= 0) {

			int pc = instance->pendingProgramChange;
			int msb = instance->pendingBankMSB;
			int lsb = instance->pendingBankLSB;

			//!!! gosh, I don't know this -- need to check with the specs:
			// if you only send one of MSB/LSB controllers, should the
			// other go to zero or remain as it was?  Assume it remains as
			// it was, for now.

			if (lsb >= 0) {
				if (msb >= 0) {
					instance->currentBank = lsb + 128 * msb;
				} else {
					instance->currentBank = lsb + 128 * (instance->currentBank / 128);
				}
			} else if (msb >= 0) {
				instance->currentBank = (instance->currentBank % 128) + 128 * msb;
			}

			instance->currentProgram = pc;

			instance->pendingProgramChange = -1;
			instance->pendingBankMSB = -1;
			instance->pendingBankLSB = -1;

			if (instance->plugin->descriptor->select_program) {
				instance->plugin->descriptor->select_program(instanceHandles[instance->number], instance->currentBank, instance->currentProgram);
			}
		}
	}

	for (inCount = 0; inCount < insTotal; ++inCount) {

		jack_default_audio_sample_t *buffer = jack_port_get_buffer(inputPorts[inCount], nframes);

		memcpy(pluginInputBuffers[inCount], buffer, nframes * sizeof(LADSPA_Data));
	}

	/* call run_synth() or run_multiple_synths() for all instances */

	i = 0;
	outCount = 0;

	while (i < instance_count) {

		/* no -- see comment in osc_exiting_handler */
/*
	if (instances[i].inactive) {
	    int j;
	    for (j = 0; j < instances[i].plugin->outs; ++j) {
		memset(pluginOutputBuffers[outCount + j], 0, nframes * sizeof(LADSPA_Data));
	    }
	    outCount += j;
	    ++i;
	    continue;
	}
*/
		outCount += instances[i].plugin->outs;

		if (instances[i].plugin->descriptor->run_multiple_synths) {
			instances[i].plugin->descriptor->run_multiple_synths (instances[i].plugin->instances, instanceHandles + i, nframes, instanceEventBuffers + i, instanceEventCounts + i);
			i += instances[i].plugin->instances;
		} else if (instances[i].plugin->descriptor->run_synth) {
			instances[i].plugin->descriptor->run_synth(instanceHandles[i], nframes, instanceEventBuffers[i], instanceEventCounts[i]);
			i++;
		} else if (instances[i].plugin->descriptor->LADSPA_Plugin->run) {
			instances[i].plugin->descriptor->LADSPA_Plugin->run(instanceHandles[i], nframes);
			i++;
		} else {
			dbg(0, "DSSI plugin %d has no run_multiple_synths, run_synth or run method!", i);
			i++;
		}
	}

	assert(sizeof(LADSPA_Data) == sizeof(jack_default_audio_sample_t));

	for (outCount = 0; outCount < outsTotal; ++outCount) {

		jack_default_audio_sample_t* buffer = jack_port_get_buffer(outputPorts[outCount], nframes);

		memcpy(buffer, pluginOutputBuffers[outCount], nframes * sizeof(LADSPA_Data));
	}

	return 0;
}


char*
load (const char* dllName, void** dll, int quiet) /* returns directory where dll found */
{
	static char* defaultDssiPath = 0;
	const char* dssiPath = 
#if 0
		getenv("DSSI_PATH");
#else
		PACKAGE_DATA_DIR;
#endif
	char* path;
	void* handle = 0;

	/* If the dllName is an absolute path */
	if (*dllName == '/') {
		if ((handle = dlopen(dllName, RTLD_NOW))) {  /* real-time programs should not use RTLD_LAZY */
			*dll = handle;
			path = strdup(dllName);
			return dirname(path);
		} else {
			if (!quiet) {
				dbg(0, "Cannot find DSSI or LADSPA plugin at '%s'", dllName);
			}
			return NULL;
		}
	}

	if (!dssiPath) {
		if (!defaultDssiPath) {
			const char *home = getenv("HOME");
			if (home) {
				defaultDssiPath = malloc(strlen(home) + 60);
				sprintf(defaultDssiPath, "/usr/local/lib/dssi:/usr/lib/dssi:%s/.dssi", home);
			} else {
				defaultDssiPath = strdup("/usr/local/lib/dssi:/usr/lib/dssi");
			}
		}
		dssiPath = defaultDssiPath;
		if (!quiet) {
			dbg(0, "\n%s: Warning: DSSI path not set\n%s: Defaulting to \"%s\"\n", myName, myName, dssiPath);
		}
	}

	path = strdup(dssiPath);
	dbg(0, "dssipath=%s", path);
	char* origPath = path;
	*dll = 0;

	char* element;
	while ((element = strtok(path, ":")) != 0) {

		path = 0;

		if (element[0] != '/') {
			if (!quiet) {
				dbg(0, "%s: Ignoring relative element \"%s\" in path", myName, element);
			}
			continue;
		}

		if (!quiet) {
			p(1, "%s: Looking for library \"%s\" in %s...", myName, dllName, element);
		}

		char* filePath = (char *)malloc(strlen(element) + strlen(dllName) + 2);
		sprintf(filePath, "%s/%s", element, dllName);

		if ((handle = dlopen(filePath, RTLD_NOW))) {  /* real-time programs should not use RTLD_LAZY */
			if (!quiet) dbg(0, "found");
			*dll = handle;
			free(filePath);
			path = strdup(element);
			free(origPath);
			return path;
		}

		if (!quiet) {
			char* message = dlerror();
			if (message) {
				dbg(0, "%s", message);
			} else {
				dbg(0, "not found");
			}
		}

		free(filePath);
	}

	free(origPath);
	return 0;
}


static int
instance_sort_cmp (const void *a, const void *b)
{
	d3h_instance_t *ia = (d3h_instance_t *)a;
	d3h_instance_t *ib = (d3h_instance_t *)b;

	if (ia->plugin->number != ib->plugin->number) {
		return ia->plugin->number - ib->plugin->number;
	} else {
		return ia->channel - ib->channel;
	}
}


void
startGUI (const char* directory, const char* dllName, const char* label, const char* oscUrl, const char* instanceTag)
{
	struct dirent *entry;
	char *dllBase = strdup(dllName);
	char *subpath;
	DIR *subdir;
	char *filename;
	struct stat buf;
	int fuzzy;

	if (strlen(dllBase) > 3 && !strcasecmp(dllBase + strlen(dllBase) - 3, ".so")) {
		dllBase[strlen(dllBase) - 3] = '\0';
	}

	if (*dllBase == '/') {
		subpath = dllBase;
		dllBase = strdup(strrchr(subpath, '/') + 1);
	} else {
		subpath = (char *)malloc(strlen(directory) + strlen(dllBase) + 2);
		sprintf(subpath, "%s/%s", directory, dllBase);
	}

	for (fuzzy = 0; fuzzy <= 1; ++fuzzy) {

		if (!(subdir = opendir(subpath))) {
			if (FALSE) {
				dbg(0, "%s: can't open plugin GUI directory \"%s\"", myName, subpath);
			}
			free(subpath);
			free(dllBase);
			return;
		}

	while ((entry = readdir(subdir))) {
		if (entry->d_name[0] == '.') continue;
		if (!strchr(entry->d_name, '_')) continue;

		if (fuzzy) {
			dbg(0, "checking %s against %s", entry->d_name, dllBase);
			if (strlen(entry->d_name) <= strlen(dllBase) ||
				strncmp(entry->d_name, dllBase, strlen(dllBase)) ||
				entry->d_name[strlen(dllBase)] != '_')
				continue;
		} else {
			p(0, "checking %s against %s", entry->d_name, label);
			if (strlen(entry->d_name) <= strlen(label) ||
				strncmp(entry->d_name, label, strlen(label)) ||
				entry->d_name[strlen(label)] != '_')
				continue;
		}

		filename = (char *)malloc(strlen(subpath) + strlen(entry->d_name) + 2);
		sprintf(filename, "%s/%s", subpath, entry->d_name);

		if (stat(filename, &buf)) {
			perror("stat failed");
			free(filename);
			continue;
		}

		if ((S_ISREG(buf.st_mode) || S_ISLNK(buf.st_mode)) && (buf.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH))) {

			p(0, "%s: trying to execute GUI at \"%s\"", myName, filename);

			if (fork() == 0) {
				execlp(filename, filename, oscUrl, dllName, label, instanceTag, NULL);
				perror("exec failed");
				exit(1);
			}

			free(filename);
			free(subpath);
			free(dllBase);
			return;
		}

			free(filename);
		}
	}

	p(0, "%s: no GUI found for plugin \"%s\" in \"%s/\"", myName, label, subpath);
	free(subpath);
	free(dllBase);
}


void
query_programs (d3h_instance_t *instance)
{
	int i;

	/* free old lot */
	if (instance->pluginPrograms) {
		for (i = 0; i < instance->pluginProgramCount; i++) free((void *)instance->pluginPrograms[i].Name);
		free((char *)instance->pluginPrograms);
		instance->pluginPrograms = NULL;
		instance->pluginProgramCount = 0;
	}

	instance->pendingBankLSB = -1;
	instance->pendingBankMSB = -1;
	instance->pendingProgramChange = -1;

	if (instance->plugin->descriptor->get_program && instance->plugin->descriptor->select_program) {

		/* Count the plugins first */
		for (i = 0; instance->plugin->descriptor->get_program(instanceHandles[instance->number], i); ++i);

		if (i > 0) {
			instance->pluginProgramCount = i;
			instance->pluginPrograms = (DSSI_Program_Descriptor *)malloc(i * sizeof(DSSI_Program_Descriptor));
			while (i > 0) {
				const DSSI_Program_Descriptor* descriptor;
				--i;
				descriptor = instance->plugin->descriptor->get_program(instanceHandles[instance->number], i);
				instance->pluginPrograms[i].Bank = descriptor->Bank;
				instance->pluginPrograms[i].Program = descriptor->Program;
				instance->pluginPrograms[i].Name = strdup(descriptor->Name);
				p(0, "%s: %s program %d is MIDI bank %lu program %lu, named '%s'",
						myName, instance->friendly_name, i,
						instance->pluginPrograms[i].Bank,
						instance->pluginPrograms[i].Program,
						instance->pluginPrograms[i].Name);
			}
		}
	}
}


static void
on_jack_shutdown (void* _)
{
	g_main_loop_quit(main_loop);
	exiting = 1;
}


int
main (int argc, char** argv)
{
	d3h_dll_t *dll;
	d3h_instance_t *instance;
	void *pluginObject;
	char *dllName;
	char *label;
	const char **ports;
	char *tmp;
	int i, reps, j;
	int in, out, controlIn, controlOut;
	char clientName[33];

#ifndef HAVE_GLIB_2_32
	g_type_init();
	g_thread_init(NULL);
#endif
	clear_log();

	setsid();
	sigemptyset (&_signals);
	sigaddset(&_signals, SIGHUP);
	sigaddset(&_signals, SIGINT);
	sigaddset(&_signals, SIGQUIT);
	sigaddset(&_signals, SIGPIPE);
	sigaddset(&_signals, SIGTERM);
	sigaddset(&_signals, SIGUSR1);
	sigaddset(&_signals, SIGUSR2);
	pthread_sigmask(SIG_BLOCK, &_signals, 0);

	insTotal = outsTotal = controlInsTotal = controlOutsTotal = 0;

	if (!myName) myName = strdup(argv[0]);

	/* Parse args and report usage */

	if (FALSE && argc < 2) {
		fprintf(stderr, "\nUsage: %s [-v] [-a] [-n] [-p <projdir>] [-<i>]\n", argv[0]);
		fprintf(stderr, "\n  -v        Verbose mode (0 - 2)\n");
		fprintf(stderr, "  -a        Don't autoconnect outputs to JACK physical outputs\n");
		/*
		fprintf(stderr, "  <projdir> Project directory to pass to plugin and UI\n");
		fprintf(stderr, "  <i>       Number of instances of each plugin to run (max %d total, default 1)\n", D3H_MAX_INSTANCES);
		fprintf(stderr, "  <libname> DSSI plugin library .so to load (searched for in $DSSI_PATH)\n");
		fprintf(stderr, "  <label>   Label of plugin to load from library\n");
		fprintf(stderr, "  [...]     Optionally more instance counts, plugins and labels\n");
		fprintf(stderr, "\nExample: %s -2 lib1.so -1 lib2.so%cfuzzy\n", argv[0], LABEL_SEP);
		fprintf(stderr, "  run two instances of the first plugin found in lib1.so, assigned to MIDI\n  channels 0 and 1 and connected to the first available JACK outputs, and one\n  instance of the \"fuzzy\" plugin in lib2.so with MIDI channels 2 and 3 and\n  connected to the next available JACK outputs.\n");
		*/
		return 2;
	}

	p(0, "\n>>>> %s: Starting...", myName);

	projectDirectory = NULL;

	int opt;
	while((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch(opt) {
			case 'v':
				printf("using debug level: %s\n", optarg);
				int d = atoi(optarg);
				if(d<0 || d>4) { p(0, "*** bad arg. debug=%i", d); } else debug = d;
				break;
			case 'h':
				printf(usage, argv[0]);
				exit(EXIT_SUCCESS);
				break;
			case '?':
				printf("unknown option: %c\n", optopt);
				break;
		}
	}

	reps = 1;
	for (i = 1; i < argc; i++) {

#if 0
		if (!strcmp(argv[i], "-v")) {
			verbose = 1;
			continue;
		}
#endif
		if (!strcmp(argv[i], "-a")) {
			autoconnect = FALSE;
			continue;
		}

		if (!strcmp(argv[i], "-p")) {
			if (i < argc - 1) {
				projectDirectory = argv[++i];
			} else {
				dbg(0, "%s: project directory expected after -p", myName);
				return 2;
			}
			continue;
		}
	}

	argc = 1;
	argv = g_malloc0(sizeof(char*) * 2);
	argv[0] = g_strdup("sampleplayer-dssi.so");
	for (i = 0; i < argc; i++) {

		if (instance_count >= D3H_MAX_INSTANCES) {
			dbg(0, "%s: too many plugin instances specified (max is %d)", myName, D3H_MAX_INSTANCES);
			return 2;
		}

		/* parse repetition count */
		if (argv[i][0] == '-') {
			reps = atoi(&argv[i][1]);
			if (reps > 0) {
				continue;
			} else {
				reps = 1;
			}
		}

		/* parse dll name, plus a label if supplied */
		tmp = strchr(argv[i], LABEL_SEP);
		if (tmp) {
			dllName = calloc(1, tmp - argv[i] + 1);
			strncpy(dllName, argv[i], tmp - argv[i]);
			label = strdup(tmp + 1);
		} else {
			dllName = strdup(argv[i]);
			label = NULL;
		}

		/* check if we've seen this plugin before */
		d3h_plugin_t *plugin;
		for (plugin = plugins; plugin; plugin = plugin->next) {
			if (label) {
				if (!strcmp(dllName, plugin->dll->name) && !strcmp(label, plugin->label)) break;
			} else {
				if (!strcmp(dllName, plugin->dll->name) && plugin->is_first_in_dll) break;
			}
		}

		if (plugin) {
			/* have already seen this plugin */

			free(dllName);
			free(label);

		} else {
			/* this is a new plugin */

			plugin = (d3h_plugin_t *)calloc(1, sizeof(d3h_plugin_t));
			plugin->number = plugin_count;
			plugin->label = label;

			/* check if we've seen this dll before */
			for (dll = dlls; dll; dll = dll->next) {
				if (!strcmp(dllName, dll->name)) break;
			}
			if (!dll) {
				/* this is a new dll */
				dll = (d3h_dll_t *)calloc(1, sizeof(d3h_dll_t));
				dll->name = dllName;
				//printf("name=%s\n", dll->name);

				dll->directory = load(dllName, &pluginObject, 0);
				if (!dll->directory || !pluginObject) {
					dbg(0, "\n%s: Error: Failed to load plugin library \"%s\"", dllName);
					return 1;
				}

				dll->descfn = (DSSI_Descriptor_Function)dlsym(pluginObject, "dssi_descriptor");
				if (dll->descfn) {
					dll->is_DSSI_dll = 1;
				} else {
					dbg(0, "dssi_descriptor not found");
					dll->descfn = (DSSI_Descriptor_Function)dlsym(pluginObject, "ladspa_descriptor");
					if (!dll->descfn) {
						dbg(0, "\nError: \"%s\" is not a DSSI or LADSPA plugin library", dllName);
						return 1;
					}
					dll->is_DSSI_dll = 0;
				}

				dll->next = dlls;
				dlls = dll;
			}
			plugin->dll = dll;

			/* get the plugin descriptor */
			j = 0;
			if (dll->is_DSSI_dll) {
				const DSSI_Descriptor* desc;
				while ((desc = dll->descfn(j++))) {
					if (!plugin->label || !strcmp(desc->LADSPA_Plugin->Label, plugin->label)) {
						plugin->descriptor = desc;
						((DSSI_Descriptor*)plugin->descriptor)->receive_host_descriptor(&host_descriptor);
						break;
					}
				}
			} else { /* LADSPA plugin; create and use a dummy DSSI descriptor */
				printf("**** never get here! ***\n");
				LADSPA_Descriptor *desc;

				plugin->descriptor = (const DSSI_Descriptor *)calloc(1, sizeof(DSSI_Descriptor));
				((DSSI_Descriptor*)plugin->descriptor)->DSSI_API_Version = 1;

				while ((desc = (LADSPA_Descriptor *)dll->descfn(j++))) {
					if (!plugin->label || !strcmp(desc->Label, plugin->label)) {
						((DSSI_Descriptor*)plugin->descriptor)->LADSPA_Plugin = desc;
						break;
					}
				}
				if (!plugin->descriptor->LADSPA_Plugin) {
					free((void *)plugin->descriptor);
					plugin->descriptor = NULL;
				}
			}
			if (!plugin->descriptor) {
				dbg(0, "\n%s: Error: Plugin label \"%s\" not found in library \"%s\"", myName, plugin->label ? plugin->label : "(none)", dllName);
				return 1;
			}
			plugin->is_first_in_dll = (j = 1);
			if (!plugin->label) {
				plugin->label = strdup(plugin->descriptor->LADSPA_Plugin->Label);
			}

			/* Count number of i/o buffers and ports required */
			plugin->ins = 0;
			plugin->outs = 0;
			plugin->controlIns = 0;
			plugin->controlOuts = 0;

			for (j = 0; j < plugin->descriptor->LADSPA_Plugin->PortCount; j++) {

				LADSPA_PortDescriptor pod = plugin->descriptor->LADSPA_Plugin->PortDescriptors[j];

				if (LADSPA_IS_PORT_AUDIO(pod)) {

					if (LADSPA_IS_PORT_INPUT(pod)) ++plugin->ins;
					else if (LADSPA_IS_PORT_OUTPUT(pod)) ++plugin->outs;

				} else if (LADSPA_IS_PORT_CONTROL(pod)) {

					if (LADSPA_IS_PORT_INPUT(pod)) ++plugin->controlIns;
					else if (LADSPA_IS_PORT_OUTPUT(pod)) ++plugin->controlOuts;
				}
			}

			/* finish up new plugin */
			plugin->instances = 0;
			plugin->next = plugins;
			plugins = plugin;
			plugin_count++;
		}

		/* set up instances */
		for (j = 0; j < reps; j++) {
			if (instance_count < D3H_MAX_INSTANCES) {
				instance = &instances[instance_count];

				instance->plugin = plugin;
				instance->channel = instance_count;
				instance->inactive = 1;
				tmp = (char *)malloc(strlen(plugin->dll->name) + strlen(plugin->label) + 9);
				instance->friendly_name = tmp;
				strcpy(tmp, plugin->dll->name);
				if (strlen(tmp) > 3 && !strcasecmp(tmp + strlen(tmp) - 3, ".so")) {
					tmp = tmp + strlen(tmp) - 3;
				} else {
					tmp = tmp + strlen(tmp);
				}
				sprintf(tmp, "/%s/chan%02d", plugin->label, instance->channel);
				instance->pluginProgramCount = 0;
				instance->pluginPrograms = NULL;
				instance->currentBank = 0;
				instance->currentProgram = 0;
				instance->pendingBankLSB = -1;
				instance->pendingBankMSB = -1;
				instance->pendingProgramChange = -1;
				instance->uiTarget = NULL;

				insTotal += plugin->ins;
				outsTotal += plugin->outs;
				controlInsTotal += plugin->controlIns;
				controlOutsTotal += plugin->controlOuts;

				plugin->instances++;
				instance_count++;
			} else {
				dbg(0, "%s: too many plugin instances specified", myName);
				return 2;
			}
		}
		reps = 1;
	}

	if (instance_count == 0) {
		dbg(0, "%s: No plugin specified", myName);
		return 2;
	}

	/* sort array of instances to group them by plugin */
	if (instance_count > 1) {
		qsort(instances, instance_count, sizeof(d3h_instance_t), instance_sort_cmp);
	}

	/* build channel2instance[] while showing what our instances are */
	for (i = 0; i < D3H_MAX_CHANNELS; i++)
		channel2instance[i] = NULL;
	for (i = 0; i < instance_count; i++) {
		instance = &instances[i];
		instance->number = i;
		channel2instance[instance->channel] = instance;
		dbg(0, "%s: instance %2d on channel %2d, plugin %2d is \"%s\"", myName, i, instance->channel, instance->plugin->number, instance->friendly_name);
	}

	/* Create buffers and JACK client and ports */

	if (instance_count > 1)
		strcpy(clientName, "jack-dssi-host");
	else {
		g_strlcpy(clientName, instances[0].plugin->descriptor->LADSPA_Plugin->Name, 32);
	}

	jack_status_t status;
	if ((jackClient = jack_client_open(clientName, 0, &status)) == 0) {
        dbg(0, "\n%s: Error: Failed to connect to JACK server", myName);
        return 1;
	}
	if (status & JackNameNotUnique) {
		strncpy(clientName, jack_get_client_name(jackClient), 32);
		clientName[32] = '\0';
	}

    sample_rate = jack_get_sample_rate(jackClient);

    inputPorts = (jack_port_t**)malloc(insTotal * sizeof(jack_port_t *));
    pluginInputBuffers = (float**)malloc(insTotal * sizeof(float *));
    pluginControlIns = (float*)calloc(controlInsTotal, sizeof(float));
    pluginControlInInstances = (d3h_instance_t **)malloc(controlInsTotal * sizeof(d3h_instance_t *));
    pluginControlInPortNumbers = (unsigned long *)malloc(controlInsTotal * sizeof(unsigned long));
    pluginPortUpdated = (int*)malloc(controlInsTotal * sizeof(int));

    outputPorts = (jack_port_t**)malloc(outsTotal * sizeof(jack_port_t *));
    pluginOutputBuffers = (float**)malloc(outsTotal * sizeof(float *));
    pluginControlOuts = (float*)calloc(controlOutsTotal, sizeof(float));

    instanceHandles = (LADSPA_Handle*)malloc(instance_count * sizeof(LADSPA_Handle));
    instanceEventBuffers = (snd_seq_event_t**)malloc(instance_count * sizeof(snd_seq_event_t *));
    instanceEventCounts = (unsigned long *)malloc(instance_count * sizeof(unsigned long));

	for (i = 0; i < instance_count; i++) {
        instanceEventBuffers[i] = (snd_seq_event_t*)malloc(EVENT_BUFFER_SIZE * sizeof(snd_seq_event_t));
        instances[i].pluginPortControlInNumbers = (int*)malloc(instances[i].plugin->descriptor->LADSPA_Plugin->PortCount * sizeof(int));
	}

	in = 0;
	out = 0;
	reps = 0;
	for (i = 0; i < instance_count; i++) {
		if (i > 0 && !strcmp(instances[i  ].plugin->descriptor->LADSPA_Plugin->Name, instances[i-1].plugin->descriptor->LADSPA_Plugin->Name)) {
			++reps;
		} else if (i < instance_count - 1 && !strcmp(instances[i  ].plugin->descriptor->LADSPA_Plugin->Name, instances[i+1].plugin->descriptor->LADSPA_Plugin->Name)) {
			reps = 1;
		} else {
			reps = 0;
		}
		for (j = 0; j < instances[i].plugin->ins; ++j) {
			char portName[40];
			strncpy(portName, instances[i].plugin->descriptor->LADSPA_Plugin->Name, 30);
			if (reps > 0) {
				portName[25] = '\0';
				sprintf(portName + strlen(portName), " %d in_%d", reps, j + 1);
			} else {
				portName[30] = '\0';
				sprintf(portName + strlen(portName), " in_%d", j + 1);
			}
			inputPorts[in] = jack_port_register(jackClient, portName, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
			pluginInputBuffers[in] = (float*)calloc(jack_get_buffer_size(jackClient), sizeof(float));
			++in;
		}
		for (j = 0; j < instances[i].plugin->outs; ++j) {
			char portName[40];
			strncpy(portName, instances[i].plugin->descriptor->LADSPA_Plugin->Name, 30);
				if (reps > 0) {
					portName[25] = '\0';
					sprintf(portName + strlen(portName), " %d out_%d", reps, j + 1);
				} else {
					portName[30] = '\0';
					sprintf(portName + strlen(portName), " out_%d", j + 1);
				}
				outputPorts[out] = jack_port_register(jackClient, portName, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
				pluginOutputBuffers[out] = (float*)calloc(jack_get_buffer_size(jackClient), sizeof(float));
				++out;
			}
		}
    
		jack_set_process_callback(jackClient, audio_callback, 0);

		/* Instantiate plugins */

	d3h_plugin_t* plugin;
	for (i = 0; i < instance_count; i++) {
        plugin = instances[i].plugin;
        instanceHandles[i] = plugin->descriptor->LADSPA_Plugin->instantiate(plugin->descriptor->LADSPA_Plugin, sample_rate);
        if (!instanceHandles[i]) {
            dbg(0, "\n%s: Error: Failed to instantiate instance %d!, plugin \"%s\"", myName, i, plugin->label);
            return 1;
		}
		if (projectDirectory && plugin->descriptor->configure) {
			char* rv = plugin->descriptor->configure(instanceHandles[i], DSSI_PROJECT_DIRECTORY_KEY, projectDirectory);
			if (rv) {
				dbg(0, "%s: Warning: plugin doesn't like project directory: \"%s\"", myName, rv);
			}
		}
	}

	start_dbus();

#ifdef USE_OSC
	/* Create OSC thread */

	serverThread = lo_server_thread_new(NULL, osc_error);
	snprintf((char*)osc_path_tmp, 31, "/dssi");
	tmp = lo_server_thread_get_url(serverThread);
	char* url = (char*)malloc(strlen(tmp) + strlen(osc_path_tmp));
	sprintf(url, "%s%s", tmp, osc_path_tmp + 1);
	p(0, "%s: registering %s", myName, url);
	free(tmp);

	lo_server_thread_add_method(serverThread, NULL, NULL, osc_message_handler, NULL);
	lo_server_thread_start(serverThread);
#endif

	/* Connect and activate plugins */

	for (in = 0; in < controlInsTotal; in++) {
		pluginPortUpdated[in] = 0;
	}

	in = out = controlIn = controlOut = 0;

	for (i = 0; i < instance_count; i++) {   /* i is instance number */
		instance = &instances[i];

		instance->firstControlIn = controlIn;
		for (j = 0; j < MIDI_CONTROLLER_COUNT; j++) {
			instance->controllerMap[j] = -1;
		}

		plugin = instance->plugin;
		for (j = 0; j < plugin->descriptor->LADSPA_Plugin->PortCount; j++) {  /* j is LADSPA port number */
			dbg(2, "port=%i", j);

			LADSPA_PortDescriptor pod = plugin->descriptor->LADSPA_Plugin->PortDescriptors[j];

			instance->pluginPortControlInNumbers[j] = -1;

			if (LADSPA_IS_PORT_AUDIO(pod)) {

				if (LADSPA_IS_PORT_INPUT(pod)) {
					plugin->descriptor->LADSPA_Plugin->connect_port(instanceHandles[i], j, pluginInputBuffers[in++]);

				} else if (LADSPA_IS_PORT_OUTPUT(pod)) {
					plugin->descriptor->LADSPA_Plugin->connect_port(instanceHandles[i], j, pluginOutputBuffers[out++]);
				}

			} else if (LADSPA_IS_PORT_CONTROL(pod)) {

				if (LADSPA_IS_PORT_INPUT(pod)) {

					if (plugin->descriptor->get_midi_controller_for_port) {

						int controller = plugin->descriptor->get_midi_controller_for_port(instanceHandles[i], j);

						if (controller == 0) {
							MB_MESSAGE ("Buggy plugin: wants mapping for bank MSB\n");
						} else if (controller == 32) {
							MB_MESSAGE ("Buggy plugin: wants mapping for bank LSB\n");
						} else if (DSSI_IS_CC(controller)) {
							instance->controllerMap[DSSI_CC_NUMBER(controller)] = controlIn;
						}
					}

					pluginControlInInstances[controlIn] = instance;
					pluginControlInPortNumbers[controlIn] = j;
					instance->pluginPortControlInNumbers[j] = controlIn;

					pluginControlIns[controlIn] = get_port_default(plugin->descriptor->LADSPA_Plugin, j);

					plugin->descriptor->LADSPA_Plugin->connect_port(instanceHandles[i], j, &pluginControlIns[controlIn++]);

				} else if (LADSPA_IS_PORT_OUTPUT(pod)) {
					plugin->descriptor->LADSPA_Plugin->connect_port(instanceHandles[i], j, &pluginControlOuts[controlOut++]);
				}
			}
		}  /* 'for (j...'  LADSPA port number */

		if (plugin->descriptor->LADSPA_Plugin->activate) {
			plugin->descriptor->LADSPA_Plugin->activate(instanceHandles[i]);
		}
		instance->inactive = 0;
	} /* 'for (i...' instance number */

	assert(in == insTotal);
	assert(out == outsTotal);
	assert(controlIn == controlInsTotal);
	assert(controlOut == controlOutsTotal);

	/* Look up synth programs */

	for (i = 0; i < instance_count; i++) {
		instance = &instances[i];

		query_programs(instance);
		
		if (instance->plugin->descriptor->select_program && instance->pluginProgramCount > 0) {

			/* select program at index 0 */
			unsigned long bank = instance->pluginPrograms[0].Bank;
			instance->pendingBankMSB = bank / 128;
			instance->pendingBankLSB = bank % 128;
			instance->pendingProgramChange = instance->pluginPrograms[0].Program;
		}
	}

	for (i = 0; i < instance_count; i++) {
		instance = &instances[i];

		//TODO maybe want to move this to later...

		/////////////////////////////////////////

		instance->plugin->descriptor->configure(instanceHandles[i], "sampledir", PACKAGE_DATA_DIR);

		int p_ = 0;
		const DSSI_Program_Descriptor* desc = instance->plugin->descriptor->get_program(instanceHandles[i], p_);
		if(desc){
			p(0, "program=%i,%s", p_, desc->Name);
			instance->plugin->descriptor->select_program(instanceHandles[i], desc->Bank, desc->Program);
		}
		else printf("cannot get program 0\n");

		/////////////////////////////////////////

		break; //we only have 1 anyway.
	}

	/* Create ALSA MIDI port */

#ifdef MIDI_ALSA
	if (snd_seq_open(&alsa_client, "hw", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
		fprintf(stderr, "\n%s: Error: Failed to open ALSA sequencer interface\n", myName);
		return 1;
	}

	snd_seq_set_client_name(alsa_client, clientName);

	int portid;
	if ((portid = snd_seq_create_simple_port(alsa_client, clientName, SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE, 0)) < 0) {
		fprintf(stderr, "\n%s: Error: Failed to create ALSA sequencer port\n", myName);
		return 1;
	}

	int npfd = snd_seq_poll_descriptors_count(alsa_client, POLLIN);
	static struct pollfd* pfd;
	pfd = (struct pollfd*)alloca(npfd * sizeof(struct pollfd));
	snd_seq_poll_descriptors(alsa_client, pfd, npfd, POLLIN);
#endif /* MIDI_ALSA */

	mb_init("host: ");

	jack_on_shutdown (jackClient, on_jack_shutdown, NULL);

	/* activate JACK and connect ports */
	if (jack_activate(jackClient)) {
		dbg(0, "cannot activate jack client");
		return EXIT_FAILURE;
	}

	if (autoconnect) {
		ports = jack_get_ports(jackClient, NULL, "^" JACK_DEFAULT_AUDIO_TYPE "$", JackPortIsPhysical|JackPortIsInput);
		dbg(0, "autoconnect: n_outs=%i", outsTotal);
		if (ports && ports[0]) {
			int out = 0;
			for (out = 0, j = 0; out < outsTotal; ++out) {
				if (jack_connect(jackClient, jack_port_name(outputPorts[out]), ports[j])) {
					dbg(0, "cannot connect output port %d", out);
				}
				if (!ports[++j]) j = 0;
			}
			free(ports);
		}
	}

	signal(SIGINT, signalHandler);
	signal(SIGTERM, signalHandler);
	signal(SIGHUP, signalHandler);
	signal(SIGQUIT, signalHandler);
	pthread_sigmask(SIG_UNBLOCK, &_signals, 0);

	p(0, "Ready");

	gboolean
	alsa_source_prepare (GSource* source, gint* timeout)
	{
		*timeout = -1; // dont mind how long the poll blocks
		gboolean ready = false;
		return ready;
	}

	gboolean
	alsa_source_check (GSource* source)
	{
		// Called after all the file descriptors are polled.
		// The source should return TRUE if it is ready to be dispatched

		return pfd->revents;
	}

	gboolean
	alsa_source_dispatch (GSource* source, GSourceFunc callback, gpointer user_data)
	{
		if (!callback) return FALSE;
		callback(NULL);
		return true;
	}

	void
	alsa_source_finalize (GSource* source)
	{
		// Called when the source is finalized
	}

	GSourceFuncs alsa_poll_funcs = {
		alsa_source_prepare,
		alsa_source_check,
		alsa_source_dispatch,
		alsa_source_finalize
	};

	GSource* source = g_source_new(&alsa_poll_funcs, sizeof(GSource));

	g_source_add_poll(source, (GPollFD*)pfd);
	g_source_add_poll(source, (GPollFD*)pfd);

	gboolean callback(gpointer data)
	{
		midi_callback(data);
		return G_SOURCE_CONTINUE;
	}
	g_source_set_callback (source, callback, NULL, NULL);

	g_source_attach(source, NULL);
	g_main_loop_run (main_loop = g_main_loop_new (NULL, 0));

#if NO_GLIB_MAIN_LOOP
	exiting = 0;

	while (!exiting) {

		if (poll(pfd, npfd, 100) > 0) {
			midi_callback(NULL);
		}

		/* Race conditions here, because the programs and ports are
		   updated from the audio thread.  We at least try to minimise
		   trouble by copying out before the expensive OSC call */

		for (i = 0; i < instance_count; i++) {
			instance = &instances[i];
			if (instance->uiNeedsProgramUpdate && instance->pendingProgramChange < 0) {
#ifdef USE_OSC
				int bank = instance->currentBank;
				int program = instance->currentProgram;
#endif
				instance->uiNeedsProgramUpdate = 0;
#ifdef USE_OSC
				if (instance->uiTarget) {
					lo_send(instance->uiTarget, instance->ui_osc_program_path, "ii", bank, program);
				}
#endif
			}
		}

		for (i = 0; i < controlInsTotal; ++i) {
			if (pluginPortUpdated[i]) {
#ifdef USE_OSC
				int port = pluginControlInPortNumbers[i];
				float value = pluginControlIns[i];
#endif
				instance = pluginControlInInstances[i];
				pluginPortUpdated[i] = 0;
#ifdef USE_OSC
				if (instance->uiTarget) {
					lo_send(instance->uiTarget, instance->ui_osc_control_path, "if", port, value);
				}
#endif
			}
		}
	}
#endif

	jack_client_close(jackClient);

	/* cleanup plugins */
	for (i = 0; i < instance_count; i++) {
		instance = &instances[i];

		if (instance->uiTarget) {
#ifdef USE_OSC
			lo_send(instance->uiTarget, instance->ui_osc_quit_path, "");
			lo_address_free(instance->uiTarget);
#endif
			instance->uiTarget = NULL;
		}

		if (instance->plugin->descriptor->LADSPA_Plugin->deactivate) {
			instance->plugin->descriptor->LADSPA_Plugin->deactivate(instanceHandles[i]);
		}

		if (instance->plugin->descriptor->LADSPA_Plugin->cleanup) {
			instance->plugin->descriptor->LADSPA_Plugin->cleanup(instanceHandles[i]);
		}
	}

	sleep(1);
	sigemptyset (&_signals);
	sigaddset(&_signals, SIGHUP);
	pthread_sigmask(SIG_BLOCK, &_signals, 0);
	kill(0, SIGHUP);

	return EXIT_SUCCESS;
}


static void
start_dbus ()
{
#ifdef DBUS_HAS_OWN_THREAD
	msg_queue = g_async_queue_new();

	gpointer dbus_thread(gpointer data)
	{
		dbg(0, "...");

		GMainContext* context = g_main_context_new();

		/* Start Dbus server */
		AuditionerDbus* bus = auditioner_dbus_get_instance();
		if (!auditioner_dbus_register_service (bus, context)) {
			dbg(0, "dbus registration failed!");
			exit(EXIT_FAILURE);
		}

		g_async_queue_ref(msg_queue);

		gboolean worker_timeout(gpointer data)
		{

			//check for new work
			while(g_async_queue_length(msg_queue)){
				char* message = g_async_queue_pop(msg_queue); // blocks
				dbg(0, "new message! '%s'", message);
				play_note(message, NULL);
			}
			return G_SOURCE_CONTINUE;
		}

		GSource* source = g_timeout_source_new(500);
		gpointer _data = NULL;
		g_source_set_callback(source, worker_timeout, _data, NULL);
		g_source_attach(source, context);

		g_main_loop_run (g_main_loop_new (context, 0));
		return NULL;
	}

	// TODO its probably a bad idea to have the dbus code in a separate thread.
    GError* error = NULL;
    if(!g_thread_create(dbus_thread, NULL, FALSE, &error)){
        perr("error creating thread: %s\n", error->message);
        g_error_free(error);
    }
#else
	AuditionerDbus* bus = auditioner_dbus_get_instance();
	if (!auditioner_dbus_register_service (bus, NULL)) {
		dbg(0, "dbus registration failed!");
		exit(EXIT_FAILURE);
	}
#endif
	}
LADSPA_Data
get_port_default (const LADSPA_Descriptor *plugin, int port)
{
	LADSPA_PortRangeHint hint = plugin->PortRangeHints[port];
	float lower = hint.LowerBound * (LADSPA_IS_HINT_SAMPLE_RATE(hint.HintDescriptor) ? sample_rate : 1.0f);
	float upper = hint.UpperBound * (LADSPA_IS_HINT_SAMPLE_RATE(hint.HintDescriptor) ? sample_rate : 1.0f);

	if (!LADSPA_IS_HINT_HAS_DEFAULT(hint.HintDescriptor)) {
		if (!LADSPA_IS_HINT_BOUNDED_BELOW(hint.HintDescriptor) ||
			!LADSPA_IS_HINT_BOUNDED_ABOVE(hint.HintDescriptor)) {
			/* No hint, its not bounded, wild guess */
			return 0.0f;
		}

		if (lower <= 0.0f && upper >= 0.0f) {
			/* It spans 0.0, 0.0 is often a good guess */
			return 0.0f;
		}

		/* No clues, return minimum */
		return lower;
	}

	/* Try all the easy ones */

	if (LADSPA_IS_HINT_DEFAULT_0(hint.HintDescriptor)) {
		return 0.0f;
	} else if (LADSPA_IS_HINT_DEFAULT_1(hint.HintDescriptor)) {
		return 1.0f;
	} else if (LADSPA_IS_HINT_DEFAULT_100(hint.HintDescriptor)) {
		return 100.0f;
	} else if (LADSPA_IS_HINT_DEFAULT_440(hint.HintDescriptor)) {
		return 440.0f;
	}

    /* All the others require some bounds */

    if (LADSPA_IS_HINT_BOUNDED_BELOW(hint.HintDescriptor)) {
	if (LADSPA_IS_HINT_DEFAULT_MINIMUM(hint.HintDescriptor)) {
	    return lower;
	}
    }
    if (LADSPA_IS_HINT_BOUNDED_ABOVE(hint.HintDescriptor)) {
	if (LADSPA_IS_HINT_DEFAULT_MAXIMUM(hint.HintDescriptor)) {
	    return upper;
	}
	if (LADSPA_IS_HINT_BOUNDED_BELOW(hint.HintDescriptor)) {
            if (LADSPA_IS_HINT_LOGARITHMIC(hint.HintDescriptor) &&
                lower > 0.0f && upper > 0.0f) {
                if (LADSPA_IS_HINT_DEFAULT_LOW(hint.HintDescriptor)) {
                    return expf(logf(lower) * 0.75f + logf(upper) * 0.25f);
                } else if (LADSPA_IS_HINT_DEFAULT_MIDDLE(hint.HintDescriptor)) {
                    return expf(logf(lower) * 0.5f + logf(upper) * 0.5f);
                } else if (LADSPA_IS_HINT_DEFAULT_HIGH(hint.HintDescriptor)) {
                    return expf(logf(lower) * 0.25f + logf(upper) * 0.75f);
                }
            } else {
                if (LADSPA_IS_HINT_DEFAULT_LOW(hint.HintDescriptor)) {
                    return lower * 0.75f + upper * 0.25f;
                } else if (LADSPA_IS_HINT_DEFAULT_MIDDLE(hint.HintDescriptor)) {
                    return lower * 0.5f + upper * 0.5f;
                } else if (LADSPA_IS_HINT_DEFAULT_HIGH(hint.HintDescriptor)) {
                    return lower * 0.25f + upper * 0.75f;
                }
	    }
	}
    }

    /* fallback */
    return 0.0f;
}


#ifdef USE_OSC
static void
osc_error (int num, const char *msg, const char *path)
{
	dbg(0, "%s: liblo server error %d in path %s: %s", myName, num, path, msg);
}


#include "osc.c"
#endif


static int
find_program_by_name (const char* name)
{
	d3h_instance_t* instance = &instances[0];

	int p; for(p=0;p<128;p++){
		const DSSI_Program_Descriptor* desc = instance->plugin->descriptor->get_program(instanceHandles[0], p);
		if(desc){
			dbg(2, "  %s", desc->Name);
			if(!strcmp(desc->Name, name)) return p;
		}
	}
	dbg(1, "file not found '%s'", name);
	return -1;
}


#define MIDI_NOTE_ON 0x9
#define MIDI_NOTE_OFF 0x8

static gboolean
send_note (gpointer data)
{
	// Note can be either ON or OFF. fixed at middle C velocity 100.

	d3h_instance_t* instance = &instances[0];

	int event_type = GPOINTER_TO_INT(data);

	dbg(0, "send_note %s", event_type == MIDI_NOTE_ON ? "ON" : "OFF");

	static snd_midi_event_t* alsa_coder = NULL;
	static snd_seq_event_t alsaEncodeBuffer[10];
	snd_seq_event_t* ev = &alsaEncodeBuffer[0];

	if (snd_midi_event_new(10, &alsa_coder)) {
		dbg(0, "%s: Failed to initialise ALSA MIDI coder!", myName);
		return false;
	}

	snd_midi_event_reset_encode(alsa_coder);

	#define VELOCITY 100
	const unsigned char buf1[4] = {MIDI_NOTE_ON << 4, 60, VELOCITY, 0};
	const unsigned char buf2[4] = {MIDI_NOTE_OFF << 4, 60, 0, 0};
	unsigned char* buf = (unsigned char*)buf1;
	switch(event_type){
		case MIDI_NOTE_ON:
			break;
		case MIDI_NOTE_OFF:
			buf = (unsigned char*)buf2;
			//buf[0] = MIDI_NOTE_OFF << 4;
			break;
		default:
			dbg(0, "unexpected midi event type");
			return false;
	}
	long count = snd_midi_event_encode(alsa_coder, buf, 3, alsaEncodeBuffer);

	if (!count || !snd_seq_ev_is_channel_type(ev)) {
		dbg(0, "** alsa event encode failed.");
		return false;
	}

	/* substitute correct MIDI channel */
	ev->data.note.channel = instance->channel;
	
	if (ev->type == SND_SEQ_EVENT_NOTEON && ev->data.note.velocity == 0) {
		ev->type =  SND_SEQ_EVENT_NOTEOFF;
	}
		
	pthread_mutex_lock(&midiEventBufferMutex);

	if (midiEventReadIndex == midiEventWriteIndex + 1) {

		dbg(0, "%s: Warning: MIDI event buffer overflow!", myName);

	} else if (ev->type == SND_SEQ_EVENT_CONTROLLER &&
			   (ev->data.control.param == 0 || ev->data.control.param == 32)) {

		dbg(0, "%s: Warning: %s UI sent bank select controller (should use /program OSC call), ignoring", myName, instance->friendly_name);

	} else if (ev->type == SND_SEQ_EVENT_PGMCHANGE) {

		fprintf(stderr, "%s: Warning: %s UI sent program change (should use /program OSC call), ignoring\n", myName, instance->friendly_name);

	} else {

		dbg(0, "send_note: writing note event...");
		midiEventBuffer[midiEventWriteIndex] = *ev;
		midiEventWriteIndex = (midiEventWriteIndex + 1) % EVENT_BUFFER_SIZE;
	}

	pthread_mutex_unlock(&midiEventBufferMutex);

	return true;
}


gboolean
play_note (const char* name, GError** error)
{
	pchar(0, "%s--->%s", green_r, white);

	d3h_instance_t* instance = &instances[0];

	char* dirname = g_path_get_dirname(name);
	char* basename = g_path_get_basename(name);
	dbg(2, "dir=%s base=%s", dirname, basename);
	dbg(0, "%s", basename);

	char* r;
	if((r = instance->plugin->descriptor->configure(instanceHandles[0], "sampledir", dirname))){
		dbg(0, "error: %s", r);
		g_free(r);
		return false;
	}

	int p = find_program_by_name(basename);
	if(p < 0){
		if(error) *error = g_error_new(G_FILE_ERROR, 3536, "file not found");
		return false;
	}

	const DSSI_Program_Descriptor* desc = instance->plugin->descriptor->get_program(instanceHandles[0], p);
	if(desc){
		dbg(1, "program=%lu,%i,%s", desc->Bank, p, desc->Name);
		instance->plugin->descriptor->select_program(instanceHandles[0], desc->Bank, desc->Program);
	}
	else dbg(0, "cannot get program 0");

	g_free(dirname);
	g_free(basename);

	playing = true;

	return send_note(GINT_TO_POINTER(MIDI_NOTE_ON));
}


gboolean
stop_note (const char* name)
{
	pchar(0, "%s--->%s", blue_r, white);
	dbg(0, "...");
	playing = false;
	return send_note(GINT_TO_POINTER(MIDI_NOTE_OFF));
}


gboolean
queue_note (const char* name)
{
	dbg(0, "...");
	return playing
		? play_queue = g_list_append(play_queue, (char*)name), true
		: play_note(name, NULL);
}


gboolean
next_note ()
{
	next(NULL);

	return TRUE;
}


char*
get_status (int* queue_size)
{
	if(playing){
		*queue_size = g_list_length(play_queue);
		const DSSI_Program_Descriptor* desc = instances[0].plugin->descriptor->get_program(instanceHandles[0], instances[0].currentProgram);
		return g_strdup(desc->Name);
	} else {
		return g_strdup("stopped");
	}
}


