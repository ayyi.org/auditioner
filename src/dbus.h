/*
  This file is part of the Ayyi project. http://ayyi.org
  copyright (C) 2011-2018 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 3.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#include <glib-object.h>
#include <dbus/dbus-glib-bindings.h>

#define APPLICATION_SERVICE_NAME "org.ayyi.Auditioner.Daemon"
#define DBUS_APP_PATH            "/org/ayyi/auditioner/daemon"
#define DBUS_INTERFACE           "org.ayyi.auditioner.Daemon"

G_BEGIN_DECLS

typedef struct _AuditionerDbus AuditionerDbus;
typedef struct _AuditionerApplicationClass AuditionerDbusClass;

#define AUDITIONER_DBUS_TYPE_APPLICATION              (auditioner_dbus_get_type ())
#define AUDITIONER_DBUS_APPLICATION(object)           (G_TYPE_CHECK_INSTANCE_CAST((object), AUDITIONER_DBUS_TYPE_APPLICATION, AuditionerDbus))
#define AUDITIONER_DBUS_APPLICATION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST((klass), AUDITIONER_DBUS_TYPE_APPLICATION, AuditionerDbusClass))
#define AUDITIONER_DBUS_IS_APPLICATION(object)        (G_TYPE_CHECK_INSTANCE_TYPE((object), AUDITIONER_DBUS_TYPE_APPLICATION))
#define AUDITIONER_DBUS_IS_APPLICATION_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE((klass), AUDITIONER_DBUS_TYPE_APPLICATION))
#define AUDITIONER_DBUS_APPLICATION_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS((object), AUDITIONER_DBUS_TYPE_APPLICATION, AuditionerDbusClass))

#define AUDITIONER_DBUS_APP                           (auditioner_dbus_get_instance ())

struct _AuditionerDbus {
	GObject base_instance;
	DBusGConnection *connection;
};

struct _AuditionerApplicationClass {
	GObjectClass base_class;
};

GType           auditioner_dbus_get_type         ();
AuditionerDbus* auditioner_dbus_get_instance     ();
gboolean        auditioner_dbus_register_service (AuditionerDbus*, GMainContext*);

gboolean        auditioner_dbus_emit_stopped     (AuditionerDbus*, GError**);
gboolean        auditioner_dbus_emit_position    (AuditionerDbus*, int, double, GError**);

//the dbus service:
gboolean        auditioner_dbus_start            (AuditionerDbus*, const char*, GError**);
gboolean        auditioner_dbus_stop             (AuditionerDbus*, const char*, GError**);
gboolean        auditioner_dbus_queue            (AuditionerDbus*, const char*, GError**);
gboolean        auditioner_dbus_next             (AuditionerDbus*, GError**);
gboolean        auditioner_dbus_get_status       (AuditionerDbus*, char**, int*, GError**);

GQuark          auditioner_dbus_error_quark      ();

G_END_DECLS
