#include "glib.h"

typedef struct _non_rt_plugin_thread
{
	LADSPA_Handle handle;
	void          (*runFunction)(LADSPA_Handle);
	gboolean      exiting;
} NonRTPluginThread;

