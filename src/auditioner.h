/*
  This file is part of the Ayyi project. http://ayyi.org
  copyright (C) 2011 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/* based on jack-dssi-host.h
 *
 * Copyright 2004, 2009 Chris Cannam, Steve Harris and Sean Bolton.
 * 
 * Permission to use, copy, modify, distribute, and sell this software
 * for any purpose is hereby granted without fee, provided that the
 * above copyright notice and this permission notice are included in
 * all copies or substantial portions of the software.
 */

#ifndef __auditioner_h__
#define __auditioner_h__

#include "dssi.h"
#include <lo/lo.h>
#include "non_rt_plugin_thread.h"

#define D3H_MAX_CHANNELS   16  /* MIDI limit */
#define D3H_MAX_INSTANCES  (D3H_MAX_CHANNELS)

/* character used to seperate DSO names from plugin labels on command line */
#define LABEL_SEP ':'

typedef struct _d3h_dll_t d3h_dll_t;

struct _d3h_dll_t
{
    d3h_dll_t*               next;
    char*                    name;
    char*                    directory;
    int                      is_DSSI_dll;
    DSSI_Descriptor_Function descfn;      /* if is_DSSI_dll is false, this is a LADSPA_Descriptor_Function */
};

typedef struct _d3h_plugin_t d3h_plugin_t;

struct _d3h_plugin_t
{
    d3h_plugin_t          *next;
    int                    number;
    d3h_dll_t             *dll;
    char                  *label;
    int                    is_first_in_dll;
    const DSSI_Descriptor *descriptor;
    int                    ins;
    int                    outs;
    int                    controlIns;
    int                    controlOuts;
    int                    instances;
};

typedef struct _d3h_instance_t d3h_instance_t;

#define MIDI_CONTROLLER_COUNT 128

struct _d3h_instance_t
{
    int              number;
    d3h_plugin_t    *plugin;
    int              channel;
    int              inactive;
    char            *friendly_name;
    int              firstControlIn;                       /* the offset to translate instance control in # to global control in # */
    int             *pluginPortControlInNumbers;           /* maps instance LADSPA port # to global control in # */
    long             controllerMap[MIDI_CONTROLLER_COUNT]; /* maps MIDI controller to global control in # */

    int              pluginProgramCount;
    DSSI_Program_Descriptor
                    *pluginPrograms;
    long             currentBank;
    long             currentProgram;
    int              pendingBankLSB;
    int              pendingBankMSB;
    int              pendingProgramChange;

    lo_address       uiTarget;
};

gboolean play_note  (const char*, GError**);
gboolean stop_note  (const char*);
gboolean queue_note (const char*);
gboolean next_note  ();
char*    get_status (int*);

#endif

