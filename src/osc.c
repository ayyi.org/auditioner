
int
osc_midi_handler(d3h_instance_t* instance, lo_arg** argv)
{
	static snd_midi_event_t* alsa_coder = NULL;
	static snd_seq_event_t alsaEncodeBuffer[10];
	snd_seq_event_t* ev = &alsaEncodeBuffer[0];

	p(0, "%s: OSC: got midi request for %s " "(%02x %02x %02x %02x)", myName, instance->friendly_name, argv[0]->m[0], argv[0]->m[1], argv[0]->m[2], argv[0]->m[3]);

	if (!alsa_coder) {
		if (snd_midi_event_new(10, &alsa_coder)) {
			fprintf(stderr, "%s: Failed to initialise ALSA MIDI coder!\n", myName);
			return 0;
		}
	}

    snd_midi_event_reset_encode(alsa_coder);

    long count = snd_midi_event_encode(alsa_coder, (argv[0]->m) + 1, 3, alsaEncodeBuffer); /* ignore OSC "port id" in argv[0]->m[0] */

    if (!count || !snd_seq_ev_is_channel_type(ev)) {
        return 0;
    }

    /* substitute correct MIDI channel */
    ev->data.note.channel = instance->channel;
    
    if (ev->type == SND_SEQ_EVENT_NOTEON && ev->data.note.velocity == 0) {
        ev->type =  SND_SEQ_EVENT_NOTEOFF;
    }
        
    pthread_mutex_lock(&midiEventBufferMutex);

    if (midiEventReadIndex == midiEventWriteIndex + 1) {

        fprintf(stderr, "%s: Warning: MIDI event buffer overflow!\n", myName);

    } else if (ev->type == SND_SEQ_EVENT_CONTROLLER &&
               (ev->data.control.param == 0 || ev->data.control.param == 32)) {

        fprintf(stderr, "%s: Warning: %s UI sent bank select controller (should use /program OSC call), ignoring\n", myName, instance->friendly_name);

    } else if (ev->type == SND_SEQ_EVENT_PGMCHANGE) {

        fprintf(stderr, "%s: Warning: %s UI sent program change (should use /program OSC call), ignoring\n", myName, instance->friendly_name);

    } else {

        midiEventBuffer[midiEventWriteIndex] = *ev;
        midiEventWriteIndex = (midiEventWriteIndex + 1) % EVENT_BUFFER_SIZE;

    }

    pthread_mutex_unlock(&midiEventBufferMutex);

    return 0;
}


int
osc_control_handler(d3h_instance_t *instance, lo_arg **argv)
{
    int port = argv[0]->i;
    LADSPA_Data value = argv[1]->f;

    if (port < 0 || port > instance->plugin->descriptor->LADSPA_Plugin->PortCount) {
	fprintf(stderr, "%s: OSC: %s port number (%d) is out of range\n", myName, instance->friendly_name, port);
	return 0;
    }
    if (instance->pluginPortControlInNumbers[port] == -1) {
	fprintf(stderr, "%s: OSC: %s port %d is not a control in\n",
                myName, instance->friendly_name, port);
	return 0;
    }
    pluginControlIns[instance->pluginPortControlInNumbers[port]] = value;
    if (verbose) {
	printf("%s: OSC: %s port %d = %f\n",
	       myName, instance->friendly_name, port, value);
    }
    
    return 0;
}


int
osc_program_handler(d3h_instance_t *instance, lo_arg **argv)
{
    int bank = argv[0]->i;
    int program = argv[1]->i;
    int i;
    int found = 0;

    for (i = 0; i < instance->pluginProgramCount; ++i) {
	if (instance->pluginPrograms[i].Bank == bank &&
	    instance->pluginPrograms[i].Program == program) {
	    if (verbose) {
		printf("%s: OSC: %s setting bank %d, program %d, name %s\n",
		       myName,
		       instance->friendly_name, bank, program,
		       instance->pluginPrograms[i].Name);
	    }
	    found = 1;
	    break;
	}
    }

    if (!found) {
	printf("%s: OSC: %s UI requested unknown program: bank %d, program %d: sending to plugin anyway (plugin should ignore it)\n",
	       myName, instance->friendly_name, bank, program);
    }

    instance->pendingBankMSB = bank / 128;
    instance->pendingBankLSB = bank % 128;
    instance->pendingProgramChange = program;

    return 0;
}


int
osc_configure_handler(d3h_instance_t *instance, lo_arg **argv)
{
	const char *key = (const char *)&argv[0]->s;
	const char *value = (const char *)&argv[1]->s;
	char *message;

	/* This is pretty much the simplest legal implementation of
	 * configure in a DSSI host. */

	/* The host has the option to remember the set of (key,value)
	 * pairs associated with a particular instance, so that if it
	 * wants to restore the "same" instance on another occasion it can
	 * just call configure() on it for each of those pairs and so
	 * restore state without any input from a GUI.  Any real-world GUI
	 * host will probably want to do that.  This host doesn't have any
	 * concept of restoring an instance from one run to the next, so
	 * we don't bother remembering these at all. */

	if (instance->plugin->descriptor->configure) {

		int n = instance->number;
		int m = n;

		if (!strncmp(key, DSSI_RESERVED_CONFIGURE_PREFIX, strlen(DSSI_RESERVED_CONFIGURE_PREFIX))) {
			fprintf(stderr, "%s: OSC: UI for plugin '%s' attempted to use reserved configure key \"%s\", ignoring\n", myName, instance->friendly_name, key);
			return 0;
		}

		if (instance->plugin->instances > 1 &&
			!strncmp(key, DSSI_GLOBAL_CONFIGURE_PREFIX,
				 strlen(DSSI_GLOBAL_CONFIGURE_PREFIX))) {
			while (n > 0 && instances[n-1].plugin == instances[m].plugin) --n;
			m = n + instances[n].plugin->instances - 1;
		}
		
		while (n <= m) {

			message = instances[n].plugin->descriptor->configure(instanceHandles[n], key, value);
			if (message) {
				printf("%s: on configure '%s' '%s', plugin '%s' returned error '%s'\n", myName, key, value, instance->friendly_name, message);
				free(message);
			}

			// also call back on UIs for plugins other than the one
			// that requested this:
			if (n != instance->number && instances[n].uiTarget) {
				lo_send(instances[n].uiTarget, instances[n].ui_osc_configure_path, "ss", key, value);
			}
			
			/* configure invalidates bank and program information, so we should do this again now: */
			query_programs(&instances[n]);

			++n;
		}
	}

	return 0;
}


int
osc_update_handler(d3h_instance_t *instance, lo_arg **argv, lo_address source)
{
	const char *url = (char *)&argv[0]->s;
	const char *path;
	unsigned int i;
	char *host, *port;
	const char *chost, *cport;

	if (verbose) {
		printf("%s: OSC: got update request from <%s>\n", myName, url);
	}

	if (instance->uiTarget) lo_address_free(instance->uiTarget);
	host = lo_url_get_hostname(url);
	port = lo_url_get_port(url);
	instance->uiTarget = lo_address_new(host, port);
	free(host);
	free(port);

	if (instance->uiSource) lo_address_free(instance->uiSource);
	chost = lo_address_get_hostname(source);
	cport = lo_address_get_port(source);
	instance->uiSource = lo_address_new(chost, cport);

	path = lo_url_get_path(url);

	if (instance->ui_osc_control_path) free(instance->ui_osc_control_path);
	instance->ui_osc_control_path = (char *)malloc(strlen(path) + 10);
	sprintf(instance->ui_osc_control_path, "%s/control", path);

	if (instance->ui_osc_configure_path) free(instance->ui_osc_configure_path);
	instance->ui_osc_configure_path = (char *)malloc(strlen(path) + 12);
	sprintf(instance->ui_osc_configure_path, "%s/configure", path);

	if (instance->ui_osc_program_path) free(instance->ui_osc_program_path);
	instance->ui_osc_program_path = (char *)malloc(strlen(path) + 10);
	sprintf(instance->ui_osc_program_path, "%s/program", path);

	if (instance->ui_osc_quit_path) free(instance->ui_osc_quit_path);
	instance->ui_osc_quit_path = (char *)malloc(strlen(path) + 10);
	sprintf(instance->ui_osc_quit_path, "%s/quit", path);

	if (instance->ui_osc_rate_path) free(instance->ui_osc_rate_path);
	instance->ui_osc_rate_path = (char *)malloc(strlen(path) + 13);
	sprintf(instance->ui_osc_rate_path, "%s/sample-rate", path);

	if (instance->ui_osc_show_path) free(instance->ui_osc_show_path);
	instance->ui_osc_show_path = (char *)malloc(strlen(path) + 10);
	sprintf(instance->ui_osc_show_path, "%s/show", path);

	free((char *)path);

	/* Send sample rate */
	lo_send(instance->uiTarget, instance->ui_osc_rate_path, "i", lrintf(sample_rate));

	/* At this point a more substantial host might also call
	 * configure() on the UI to set any state that it had remembered
	 * for the plugin instance.  But we don't remember state for
	 * plugin instances (see our own configure() implementation in
	 * osc_configure_handler), and so we have nothing to send except
	 * the optional project directory. */

	if (projectDirectory) {
		lo_send(instance->uiTarget, instance->ui_osc_configure_path, "ss", DSSI_PROJECT_DIRECTORY_KEY, projectDirectory);
	}

	/* Send current bank/program  (-FIX- another race...) */
	if (instance->pendingProgramChange < 0) {
		unsigned long bank = instance->currentBank;
		unsigned long program = instance->currentProgram;
		instance->uiNeedsProgramUpdate = 0;
		if (instance->uiTarget) {
			lo_send(instance->uiTarget, instance->ui_osc_program_path, "ii", bank, program);
		}
	}

	/* Send control ports */
	for (i = 0; i < instance->plugin->controlIns; i++) {
		int in = i + instance->firstControlIn;
	int port = pluginControlInPortNumbers[in];
	lo_send(instance->uiTarget, instance->ui_osc_control_path, "if", port, pluginControlIns[in]);
	/* Avoid overloading the GUI if there are lots and lots of ports */
	if ((i+1) % 50 == 0) usleep(300000);
	}

	/* Send 'show' */
	if (!instance->ui_initial_show_sent) {
		lo_send(instance->uiTarget, instance->ui_osc_show_path, "");
		instance->ui_initial_show_sent = 1;
	}

	return 0;
}


int
osc_exiting_handler(d3h_instance_t *instance, lo_arg **argv)
{
    int i;

    if (verbose) {
		dbg(0, "%s: OSC: got exiting notification for instance %d", myName, instance->number);
    }

    if (instance->uiTarget) {
        lo_address_free(instance->uiTarget);
        instance->uiTarget = NULL;
    }

    if (instance->uiSource) {
        lo_address_free(instance->uiSource);
        instance->uiSource = NULL;
    }

    if (instance->plugin) {

		/*!!! No, this isn't safe -- plugins deactivated in this way
		  would still be included in a run_multiple_synths call unless
		  we re-jigged the instance array at the same time -- leave it
		  for now
		if (instance->plugin->descriptor->LADSPA_Plugin->deactivate) {
	            instance->plugin->descriptor->LADSPA_Plugin->deactivate
			(instanceHandles[instance->number]);
		}
		*/
		/* Leave this flag though, as we need it to determine when to exit */
		instance->inactive = 1;
	}

	/* Do we have any plugins left running? */

	for (i = 0; i < instance_count; ++i) {
		if (!instances[i].inactive) return 0;
	}

	if (verbose) {
		dbg(0, "%s: That was the last remaining plugin, exiting...", myName);
	}
	exiting = 1;
	return 0;
}


int osc_debug_handler(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
    int i;

    printf("%s: got unhandled OSC message:\npath: <%s>\n", myName, path);
    for (i=0; i<argc; i++) {
        printf("%s: arg %d '%c' ", myName, i, types[i]);
        lo_arg_pp(types[i], argv[i]);
        printf("\n");
    }
    printf("%s:\n", myName);

    return 1;
}


int osc_message_handler(const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
	int i;
	d3h_instance_t *instance = NULL;
	const char *method;
	unsigned int flen = 0;
	lo_message message;
	lo_address source;
	int send_to_ui = 0;

	if (strncmp(path, "/dssi/", 6))
		return osc_debug_handler(path, types, argv, argc, data, user_data);

	for (i = 0; i < instance_count; i++) {
		flen = strlen(instances[i].friendly_name);
		if (!strncmp(path + 6, instances[i].friendly_name, flen) && *(path + 6 + flen) == '/') { /* avoid matching prefix only */
			instance = &instances[i];
			break;
		}
	}
	if (!instance) return osc_debug_handler(path, types, argv, argc, data, user_data);

	/* no -- see comment in osc_exiting_handler */
	/*
	if (instance->inactive) 
	return 0;
	*/
	method = path + 6 + flen;
	if (*method != '/' || *(method + 1) == 0)
		return osc_debug_handler(path, types, argv, argc, data, user_data);
	method++;

	message = (lo_message)data;
	source = lo_message_get_source(message);

	if (instance->uiSource && instance->uiTarget) {
		if (strcmp(lo_address_get_hostname(source), lo_address_get_hostname(instance->uiSource)) || strcmp(lo_address_get_port(source), lo_address_get_port(instance->uiSource))) {
			/* This didn't come from our known UI for this plugin, so send an update to that as well */
			send_to_ui = 1;
		}
	}

	if (!strcmp(method, "configure") && argc == 2 && !strcmp(types, "ss")) {

		if (send_to_ui) {
			lo_send(instance->uiTarget, instance->ui_osc_configure_path, "ss", &argv[0]->s, &argv[1]->s);
		}

		return osc_configure_handler(instance, argv);

	} else if (!strcmp(method, "control") && argc == 2 && !strcmp(types, "if")) {

		if (send_to_ui) {
			lo_send(instance->uiTarget, instance->ui_osc_control_path, "if", argv[0]->i, argv[1]->f);
		}

		return osc_control_handler(instance, argv);

	} else if (!strcmp(method, "midi") && argc == 1 && !strcmp(types, "m")) {

        return osc_midi_handler(instance, argv);

    } else if (!strcmp(method, "program") && argc == 2 && !strcmp(types, "ii")) {

	if (send_to_ui) {
	    lo_send(instance->uiTarget, instance->ui_osc_program_path, "ii",
		    argv[0]->i, argv[1]->i);
	}
	
        return osc_program_handler(instance, argv);

    } else if (!strcmp(method, "update") && argc == 1 && !strcmp(types, "s")) {

        return osc_update_handler(instance, argv, source);

    } else if (!strcmp(method, "exiting") && argc == 0) {

        return osc_exiting_handler(instance, argv);
    }

    return osc_debug_handler(path, types, argv, argc, data, user_data);
}
