/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __utils_c__
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <glib.h>

#include "utils.h"

int debug = 1;
bool prefer_stdout = false;


static void
_output (const char* str, gboolean newline)
{
	FILE* logfile = prefer_stdout ? NULL : fopen (LOG_DIR "/auditioner.log", "a");
	FILE* stream = logfile ? logfile : stderr;
	fprintf(stream, "%s", str);

	if(newline){
		fprintf(stream, "\n");
	}else{
		fflush(stream);
	}

	if(logfile) fclose (logfile);
}


void
debug_printf (const char* func, int level, const char* format, ...)
{
	va_list args;

	va_start(args, format);
	if (level <= debug) {

		gchar* s = g_strdup_vprintf(format, args);
		gchar* t = g_strdup_printf("%s(): %s", func, s);
		_output(t, true);
		g_free(s);
		g_free(t);
	}
	va_end(args);
}


void
errprintf (const char* format, ...)
{
	// print an error string, then pass arguments on to vprintf.

	va_list argp;
	va_start(argp, format);
	gchar* s = g_strdup_vprintf(format, argp);
	gchar* t = g_strdup_printf("%s %s", ayyi_err, s);
	_output(t, true);
	g_free(s);
	g_free(t);
	va_end(argp);
}


void
p (int level, const char* format, ...)
{
	va_list argp;
	va_start(argp, format);
	if (level <= debug) {
		gchar* s = g_strdup_vprintf(format, argp);
		_output(s, true);
		g_free(s);
	}
	va_end(argp);
}


void
pchar (int level, const char* format, ...)
{
	// has no newline added.

	va_list argp;
	va_start(argp, format);
	if (level <= debug) {
		gchar* s = g_strdup_vprintf(format, argp);
		_output(s, false);
		g_free(s);
	}
	va_end(argp);
}


void
clear_log ()
{
	FILE* logfile = prefer_stdout ? NULL : fopen (LOG_DIR "/auditioner.log", "w");
	if(logfile) fclose (logfile);
}


void
filename_get_extension(const char* path, char* extn)
{
	// extn must be less than 8 chars in length

	g_return_if_fail(path);
	g_return_if_fail(extn);

	gchar** split = g_strsplit(path, ".", 0);
	if(split[0]){
		int i = 1;
		while(split[i]) i++;
		strncpy(extn, split[i - 1], 7);

	}else{
		memset(extn, '\0', 1);
	}

	g_strfreev(split);
}


