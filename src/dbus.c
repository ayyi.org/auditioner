/*
  This file is part of the Ayyi project. http://ayyi.org
  copyright (C) 2011-2018 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  -----------------------------------------------------------------------

  to monitor signals:
    gdbus monitor -e -d org.ayyi.Auditioner.Daemon

*/
#include "config.h"
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <glib.h>
#include <glib-object.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "dbus.h"
#include "dbus-service.h"
#include "utils.h"
#include "auditioner.h"

#define P_IN printf("%s--->%s ", green, white)
#define P_OUT printf("%s(): %s--->%s\n", __func__, yellow, white)

extern GAsyncQueue* msg_queue;

enum
{
  STOPPED_SIGNAL,
  POSITION_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = {0,};

static DBusGConnection *connection = NULL;
static DBusGProxy      *proxy      = NULL;

G_DEFINE_TYPE (AuditionerDbus, auditioner_dbus, G_TYPE_OBJECT);


static void
auditioner_dbus_class_init (AuditionerDbusClass* klass)
{
	signals[STOPPED_SIGNAL] = g_signal_new ("playback_stopped",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

	signals[POSITION_SIGNAL] = g_signal_new ("position",
                  G_OBJECT_CLASS_TYPE (klass),
                  (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED),
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__INT,
                  G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_DOUBLE);
}


static void
auditioner_dbus_init (AuditionerDbus* application)
{
}


//tmp? private to dbus-glib
#define _DBUS_POINTER_SHIFT(p)   ((void*) (((char*)p) + sizeof (void*)))
#define _DBUS_POINTER_UNSHIFT(p) ((void*) (((char*)p) - sizeof (void*)))

#define DBUS_CONNECTION_FROM_G_CONNECTION(x)     ((DBusConnection*) _DBUS_POINTER_UNSHIFT(x))
#define DBUS_MESSAGE_FROM_G_MESSAGE(x)           ((DBusMessage*) _DBUS_POINTER_UNSHIFT(x))
#define DBUS_PENDING_CALL_FROM_G_PENDING_CALL(x) ((DBusPendingCall*) _DBUS_POINTER_UNSHIFT(x))

#define DBUS_G_CONNECTION_FROM_CONNECTION(x)     ((DBusGConnection*) _DBUS_POINTER_SHIFT(x))



gboolean
auditioner_dbus_register_service (AuditionerDbus* application, GMainContext* context)
{
	GError* err = NULL;

	if (connection) {
		g_warning ("Service already registered.");
		return FALSE;
	}

	#define BUS_COUNT 2
	struct _busses {
		DBusBusType type;
		char        name[64];
	} busses[BUS_COUNT] = {
		{DBUS_BUS_SESSION, "session"},
		{DBUS_BUS_SYSTEM, "system"}
	};

	if(context){
		//call this here to initialise generic g_dbus types.
		dbus_g_object_type_install_info (AUDITIONER_DBUS_TYPE_APPLICATION, &dbus_glib_auditioner_dbus_object_info);
	}

	dbg(2, "connecting...");
	int i; for(i=0;i<BUS_COUNT;i++){

		if(!context){
			//use regular glib main loop

			if (!(connection = dbus_g_bus_get (busses[i].type, &err))){
				dbg(0, "dbus bus connect (%i: %s) failed", i, busses[i].name);
				if(err->code != 0) dbg(0, ": %i: %s", err->code, err->message);
				dbg(0, "\n");
				g_error_free (err);
				err = NULL;
			}
			else break;

#ifdef DBUS_HAS_OWN_THREAD
		}else{
			//use the specified non-regular glib main loop
			dbg(1, "using non-recomended path");

			DBusError* dbus_error = NULL;
			DBusConnection* dbus_connection = dbus_bus_get(busses[i].type, dbus_error);
			connection = DBUS_G_CONNECTION_FROM_CONNECTION(dbus_connection);

			if (!connection){
				dbg(0, "dbus bus connect (%i: %s) failed", i, busses[i].name);
				if(err->code != 0) dbg(0, ": %i: %s", err->code, err->message);
				dbg(0, "\n");
				g_error_free (err);
				err = NULL;

			}else{
				dbus_connection_setup_with_g_main(dbus_connection, context);
				break;
			}
#endif
		}
	}
	if(!connection){ errprintf("dbus service registration %s\n", fail); return FALSE; }
	application->connection = connection;
	dbg(2, "bus=%s %s %s %s", busses[i].name, DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS);

	proxy = dbus_g_proxy_new_for_name (connection, DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS);
	if(!proxy) perr("!!");

	guint request_name_result;

	if (!org_freedesktop_DBus_request_name (proxy, APPLICATION_SERVICE_NAME, DBUS_NAME_FLAG_DO_NOT_QUEUE, &request_name_result, &err)) {
		errprintf("dbus service registration failed. %s\n", err->message);
		g_clear_error (&err);
		return FALSE;
	}
	dbg(1, "name registered: %s. result=%i", APPLICATION_SERVICE_NAME, request_name_result);

	if (request_name_result == DBUS_REQUEST_NAME_REPLY_EXISTS) {
		dbg(0, "service already running");
		return TRUE;
	}

	/*
		Install introspection information about the given object GType sufficient to allow methods on 
		the object to be invoked by name.

		The introspection information is normally generated by dbus-glib-tool, then this function is 
		called in the class_init() for the object class.

		Once introspection information has been installed, instances of the object registered with 
		dbus_g_connection_register_g_object() can have their methods invoked remotely.
	*/
	if(!context){
		dbus_g_object_type_install_info (AUDITIONER_DBUS_TYPE_APPLICATION, &dbus_glib_auditioner_dbus_object_info);
	}

	dbus_g_connection_register_g_object (connection, DBUS_APP_PATH, G_OBJECT (application));

	return TRUE;
}


AuditionerDbus*
auditioner_dbus_get_instance ()
{
	static AuditionerDbus* instance = NULL;

	return instance ? instance : (instance = AUDITIONER_DBUS_APPLICATION (g_object_new (AUDITIONER_DBUS_TYPE_APPLICATION, NULL)));
}


gboolean
auditioner_dbus_start (AuditionerDbus* auditioner, const char* name, GError** error)
{
	dbg(0, "%s", name);
#ifdef DBUS_HAS_OWN_THREAD
	g_async_queue_push(msg_queue, g_strdup((char*)name));
	return true;
#else
	return play_note(g_strdup((char*)name), error);
#endif
}


gboolean
auditioner_dbus_stop (AuditionerDbus* auditioner, const char* name, GError** error)
{
	stop_note(g_strdup((char*)name));
	return TRUE;
}


gboolean
auditioner_dbus_queue (AuditionerDbus* auditioner, const char* name, GError** error)
{
	queue_note(g_strdup((char*)name));
	return TRUE;
}


gboolean
auditioner_dbus_get_status (AuditionerDbus* auditioner, char** status, int* queue_size, GError** error)
{
	dbg(0, "got status request");
	*status = get_status(queue_size);
	return TRUE;
}


gboolean
auditioner_dbus_next (AuditionerDbus* auditioner, GError** error)
{
	dbg(0, "got Next request");
	next_note();
	return TRUE;
}


gboolean
auditioner_dbus_emit_stopped (AuditionerDbus* obj, GError** error)
{
	dbg(0, "-->");
	g_signal_emit (obj, signals[STOPPED_SIGNAL], 0);
	return TRUE;
}


gboolean
auditioner_dbus_emit_position (AuditionerDbus* obj, int position, double seconds, GError** error)
{
	dbg(2, "%i -->", position);
	g_signal_emit (obj, signals[POSITION_SIGNAL], 0, position, seconds);
	return TRUE;
}


