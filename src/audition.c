/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2004-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 |  audition.c
 |
 |  Example command line program to demonstrate usage of the
 |  Ayyi Auditioner service.
 |
 */

#include "config.h"
#include <stdio.h>
#include <libgen.h>
#include <getopt.h>
#include <glib.h>
#include <dbus/dbus-glib-bindings.h>
#ifdef USE_SYSLOG
#  include <syslog.h>
#endif
#include "utils.h"

#define APPLICATION_SERVICE_NAME "org.ayyi.Auditioner.Daemon"
#define DBUS_APP_PATH            "/org/ayyi/auditioner/daemon"
#define DBUS_INTERFACE           "org.ayyi.auditioner.Daemon"

typedef enum
{
	COMMAND_NONE,
	COMMAND_STATUS,
	COMMAND_QUEUE,
	COMMAND_STOP,
	COMMAND_NEXT,
	COMMAND_MONITOR,
} Command;

static void play_item     (DBusGProxy*, const char*, Command);
static void show_status   ();
static void stop_playback ();
static void next          ();
static void monitor       (DBusGProxy*);
static void log_handler   (const gchar* log_domain, GLogLevelFlags, const gchar*, gpointer);
static int  sort_by_name  (gconstpointer, gconstpointer);

static const struct option long_options[] = {
	{ "queue",            0, NULL, 'q' },
	{ "stop",             0, NULL, 's' },
	{ "next",             0, NULL, 'n' },
	{ "status",           0, NULL, 'i' },
	{ "monitor",          0, NULL, 'm' },
	{ "verbose",          1, NULL, 'v' },
	{ "version",          0, NULL, 'V' },
	{ "help",             0, NULL, 'h' },
};

static const char* const short_options = "qsimnv:Vh";

static const char* const usage =
	"Ayyi Audition " PACKAGE_VERSION " copyright Tim Orford 2011-2018.\n\n"
	"Usage: %s [ options ] SAMPLE(s)\n\n"
	"  specifying a directory for SAMPLE will play each file in the directory.\n\n"
	"  -q --queue     delay playback until all other samples have finished playing.\n"
	"  -s --stop      imediately stop playback.\n"
	"  -n --next      skip to next in queue.\n"
	"  -s --status    show server status information.\n"
	"  -v --verbose   show more information.\n"
	"  -V --version   print version and quit.\n"
	"  -h --help      show this usage information and quit.\n\n"
	"Ayyi Audition is an example client for use with the Ayyi Auditioner service for auditioning audio samples.\n"
	"The org.ayyi.Auditioner.Daemon service is started on-demand.\n\n"
    "Available commands are\n    - StartPlayback\n    - StopPlayback\n    - QueueForPlayback\n    - getStatus\n\n"
	"Emitted signals are\n    - PlaybackStopped\n    - Position"
	"\n";

extern int debug;


int
main (int argc, char** argv)
{
	prefer_stdout = true;
	debug = 0;
	Command command = COMMAND_NONE;

	g_log_set_handler (NULL, G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gtk", G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("Gdk", G_LOG_LEVEL_CRITICAL | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("GLib", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
	g_log_set_handler ("GLib-GObject", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);

#ifndef HAVE_GLIB_2_32
	g_type_init();
	g_thread_init(NULL);
#endif

	int opt;
	while((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch(opt) {
			case 'q':
				printf("using queue option\n");
				command = COMMAND_QUEUE;
				break;
			case 's':
				printf("stopping playback...\n");
				command = COMMAND_STOP;
				break;
			case 'i':
				p(1, "getting server status...\n");
				command = COMMAND_STATUS;
				break;
			case 'm':
				command = COMMAND_MONITOR;
				break;
			case 'n':
				command = COMMAND_NEXT;
				break;
			case 'v':
				printf("using debug level: %s\n", optarg);
				int d = atoi(optarg);
				if(d<0 || d>5) { pwarn ("bad arg. debug=%i", d); } else debug = d;
				break;
			case 'V':
				printf ("%s %s\n\n", basename(argv[0]), PACKAGE_VERSION);
				printf(
					"Copyright (C) 2011-2018 Tim Orford\n"
					"This is free software; see the source for copying conditions.  There is NO\n"
					"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\n"
				);
				return EXIT_SUCCESS;
				break;
			case 'h':
				printf(usage, argv[0]);
				return EXIT_SUCCESS;
				break;
			default:
				break;
		}
	}

#if 0
#ifdef USE_SYSLOG // just testing...
	setlogmask (LOG_UPTO (LOG_NOTICE));
	openlog ("Ayyi.Audition", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
	syslog (LOG_NOTICE, "Program started by User %d", getuid ());
	syslog (LOG_INFO, "A tree falls in a forest");
	closelog ();
#endif
#endif

	gboolean auditioner_connect(DBusGProxy** proxy)
	{
		p(1, "connecting...");
		GError* error = NULL;
		DBusGConnection* connection = dbus_g_bus_get(DBUS_BUS_SESSION, &error);
		if(!connection){
			errprintf("failed to get dbus connection\n");
			return FALSE;
		}
		if(!(*proxy = dbus_g_proxy_new_for_name (connection, APPLICATION_SERVICE_NAME, DBUS_APP_PATH, DBUS_INTERFACE))){
			errprintf("failed to get Auditioner\n");
			return FALSE;
		}

		return TRUE;
	}

	DBusGProxy* auditioner;
	if(!auditioner_connect(&auditioner)) return EXIT_FAILURE;

	switch (command){
		case COMMAND_STATUS:
			show_status(auditioner);
			return EXIT_SUCCESS;
			break;
		case COMMAND_STOP:
			stop_playback(auditioner);
			return EXIT_SUCCESS;
			break;
		case COMMAND_MONITOR:
			monitor(auditioner);
			return EXIT_SUCCESS;
		case COMMAND_NEXT:
			next(auditioner);
			return EXIT_SUCCESS;
		default:
			break;
	}

	if(!(optind < argc)){
		printf("no sample specified\n\n");
		printf(usage, argv[0]);
		return EXIT_FAILURE;
	}

	while (optind < argc) {
		play_item(auditioner, argv[optind++], command);
	}

#if 0
	static GMainLoop* loop = NULL;
	g_main_loop_run (loop = g_main_loop_new (NULL, 0));
#endif
	//sleep(2);

	return EXIT_SUCCESS;
}


static void
play_item (DBusGProxy* auditioner, const char* sample_path, Command command)
{
	printf("playing: %s\n", sample_path);

	if(sample_path[0] != '/'){
		char* c = g_get_current_dir();
		sample_path = g_build_path("/", c, sample_path, NULL);
		g_free(c);
	}

	gboolean in_array(const char* str, char** array)
	{
		int i = 0;
		while(TRUE){
			if(!array[i]) break;
			if(!strcmp(str, array[i])) return TRUE;
			i++;
		}
		return FALSE;
	}

	// play directory
	if (g_file_test(sample_path, G_FILE_TEST_IS_DIR)){
		char* blacklist[] = {"txt", "jpg", "png", "pdf", "gz", "bz2", "zip", "rar", "sfv", NULL};

		GDir* dir = g_dir_open(sample_path, 0, NULL);
		if (dir) {
			GPtrArray* array = g_ptr_array_new ();
			const char* leaf;
			while ((leaf = g_dir_read_name(dir))) {
				if (leaf[0] == '.') continue;
				gchar* filename = g_build_filename(sample_path, leaf, NULL);
				if (!g_file_test(filename, G_FILE_TEST_IS_DIR)){
					char extn[8];
					filename_get_extension(filename, extn);
					if(!in_array(extn, blacklist)){
						g_ptr_array_add (array, (gpointer)g_strdup(filename));
					}
				}
				g_free(filename);
			}
			g_dir_close(dir);

			g_ptr_array_sort (array, sort_by_name);

			void queue (gpointer data, gpointer user_data)
			{
				DBusGProxy* auditioner = user_data;
				char* filename = data;
				printf("  queuing: %s\n", g_strrstr(filename, "/") + 1);

				GError* error = NULL;
				if(!dbus_g_proxy_call(auditioner, "QueueForPlayback", &error, G_TYPE_STRING, filename, G_TYPE_INVALID, G_TYPE_INVALID)){
					printf("error: %s\n", error->message);
					g_error_free(error);
				}
			}
			g_ptr_array_foreach (array, queue, auditioner);

			g_ptr_array_free (array, TRUE);

			return;
		}
	}

	{
		GError* error = NULL;
		char* status = NULL;
		if(!dbus_g_proxy_call(auditioner, "getStatus", &error, G_TYPE_INVALID, G_TYPE_STRING, &status, G_TYPE_INVALID)){
			if(error){
				dbg(0, "error: %s", error->message);
				g_error_free(error);
			}
		}
	}

#if 1
	GError* error = NULL;
	if(!dbus_g_proxy_call(auditioner, command == COMMAND_QUEUE ? "QueueForPlayback" : "StartPlayback", &error, G_TYPE_STRING, sample_path, G_TYPE_INVALID, G_TYPE_INVALID)){
		if(error){
			printf("error: %s\n", error->message);
			g_error_free(error);
		}
		return;
	}
	dbg(1, "call ok");
#else
	p(1, "calling startPlayback...");
	dbus_g_proxy_call_no_reply(auditioner, command == COMMAND_QUEUE ? "QueueForPlayback" : "StartPlayback", G_TYPE_STRING, sample_path, G_TYPE_INVALID);
#endif
}


static void
stop_playback (DBusGProxy* auditioner)
{
	GError* error = NULL;
	dbus_g_proxy_call(auditioner, "StopPlayback", &error, G_TYPE_STRING, g_strdup("_"), G_TYPE_INVALID, G_TYPE_INVALID);
	if(error){
		p(0, "error: %s", error->message);
		g_error_free(error);
	}
}


static void
next (DBusGProxy* auditioner)
{
	GError *error = NULL;
	dbus_g_proxy_call(auditioner, "Next", &error, G_TYPE_INVALID, G_TYPE_INVALID);
	if(error){
		p(0, "error: %s", error->message);
		g_error_free(error);
	}
}


static gboolean
get_status (DBusGProxy* auditioner, char** _status, int* len)
{
	GError* error = NULL;
	char* status;
	int queue_size;
	dbus_g_proxy_call(auditioner, "getStatus", &error, G_TYPE_INVALID, G_TYPE_STRING, &status, G_TYPE_INT, &queue_size, G_TYPE_INVALID);
	if(error){
		p(0, "error: %s", error->message);
		g_error_free(error);
		*_status = NULL;
		return FALSE;
	}
	*_status = status;
	*len = queue_size;
	return TRUE;
}


static void
show_status (DBusGProxy* auditioner)
{
	p(1, "requesting status...");
	char* status;
	int len;
	if(get_status(auditioner, &status, &len)){
		p(1, "got status");
		p(0, "playing 1 of %i: %s", len, status);
	}
}


#define GO_UP "\x1b[A\x1b[00C" // go up one line, then go to column 0

static void
monitor (DBusGProxy* auditioner)
{
	static char* status;
	static int len;
	get_status(auditioner, &status, &len);
	printf("1 of %i: %s\n", len, status);

	printf("\n");

	dbus_g_proxy_add_signal(auditioner, "Position", G_TYPE_INT, G_TYPE_DOUBLE, G_TYPE_INVALID);

	void on_position(DBusGProxy* proxy, int position, double seconds, gpointer user_data)
	{
		int minutes = seconds / 60;
		int secs = seconds - minutes * 60;
		printf(GO_UP"%i %02i.%02i:%03i  \n", position, minutes, secs, (int)(1000 * (seconds - secs - minutes * 60)));
	}
	dbus_g_proxy_connect_signal (auditioner, "Position", G_CALLBACK(on_position), NULL, NULL);

	static GMainLoop* loop = NULL;
	g_main_loop_run (loop = g_main_loop_new (NULL, 0));
}


static int
sort_by_name (gconstpointer item1, gconstpointer item2)
{
	char* a = *(char**)item1;
	char* b = *(char**)item2;

	return strcmp(a, b);
}


static void
log_handler (const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data)
{
	switch(log_level){
		case G_LOG_LEVEL_CRITICAL:
			printf("%s %s\n", ayyi_err, message);
			break;
		case G_LOG_LEVEL_WARNING:
			printf("%s %s\n", ayyi_warn, message);
			break;
		default:
			printf("log_handler(): level=%i %s\n", log_level, message);
			break;
	}
}


