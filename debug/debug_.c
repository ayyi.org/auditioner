#define __debug_c__
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <glib.h>
#include "debug_.h"


int _debug_ = 0;


void
warnprintf(char* format, ...)
{
	// print a warning string, then pass arguments on to vprintf.

	printf("%s ", ayyi_warn);

	va_list argp;
	va_start(argp, format);
	vprintf(format, argp);
	va_end(argp);
}


void
warnprintf2(const char* func, char* format, ...)
{
	// print a warning string, then pass arguments on to vprintf.

	printf("%s %s(): ", ayyi_warn, func);

	va_list argp;
	va_start(argp, format);
	vprintf(format, argp);
	va_end(argp);
}


void
errprintf(char* format, ...)
{
	// print an error string, then pass arguments on to vprintf.

	printf("%s ", ayyi_err);

	va_list argp;
	va_start(argp, format);
	vprintf(format, argp);
	va_end(argp);
}


void
errprintf2(const char* func, char* format, ...)
{
	// print an error string, then pass arguments on to vprintf.

	printf("%s %s(): ", ayyi_err, func);

	va_list argp;
	va_start(argp, format);
	vprintf(format, argp);
	va_end(argp);
}


void
log_handler(const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data)
{
	switch(log_level){
		case G_LOG_LEVEL_CRITICAL:
			printf("%s %s\n", ayyi_err, message);
			break;
		case G_LOG_LEVEL_WARNING:
			printf("%s %s\n", ayyi_warn, message);
			break;
		default:
			printf("log_handler(): level=%i %s\n", log_level, message);
			break;
	}
}

