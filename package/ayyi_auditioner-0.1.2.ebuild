# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

RESTRICT="nomirror"
IUSE="debug"
DESCRIPTION="Audio sample auditioning service"
HOMEPAGE="http://ayyi.org/auditioner"
SRC_URI="http://ayyi.org/files/${P}.tar.gz"
RESTRICT="nomirror"

LICENSE="GPL-3"
KEYWORDS="x86"
SLOT="0"

DEPEND=">=media-sound/jack-audio-connection-kit-0.99
	>=media-libs/libsndfile-1.0.10
	sys-apps/dbus
	dev-libs/dbus-glib
	media-libs/ladspa-sdk
	media-libs/libsamplerate"

src_compile() {
	$(use debug && export "CFLAGS=-g")
	local myconf

	econf \
		${myconf} \
		|| die
	emake || die
}

src_install() {
	make DESTDIR=${D} install || die
	dodoc README ChangeLog
}

